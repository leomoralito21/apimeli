<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    //Route::get('/', 'HomeController@index');
   
});

Route::resource('products', 'ProductController');
Route::resource('costos', 'AddCostosController');
Route::resource('users', 'UserController');
Route::resource('sales', 'SaleController');
Route::resource('repors', 'ReportController');
Route::get('/product', 'ProductController@index');
Route::Get('/meli', 'MeliController@index');
Route::Get('/user', 'UserController@index');
Route::Get('sales', 'SaleController@index');
Route::Get('/orders', 'TokenController@consult_orders');
Route::get('/dashboard', 'TokenController@index')->name('home');
Route::get('/costo', 'AddCostosController@index')->name('costo');
Route::get('/put_costo', 'AddCostosController@store')->name('put_costo');
Route::get('/listar_costos', 'AddCostosController@show')->name('get_costo');
Route::get('/get_id_costo', 'AddCostosController@store')->name('get_ids_costo');
Route::get('/customer', 'CustomerController@create')->name('customer');
Route::get('/put_customer', 'CustomerController@store')->name('put_customer');
Route::get('/get_customer', 'CustomerController@index')->name('get_customer');
Route::get('/get_id_customer', 'CustomerController@show')->name('get_id_customer');
Route::get('/report', 'ReportController@index')->name('get_report');
Route::get('/get_report_sales', 'ReportController@store')->name('get_report_sales');
Route::Get('/invoice', 'InvoiceController@index')->name('invoices');
Route::Get('/detail_invoice', 'DetailInvoiceController@index')->name('detail_invoices');