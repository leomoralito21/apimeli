<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('client_id')->unsigned();
            $table->integer('products_id')->unsigned();
            //factura
            $table->string('code_facetura');
            $table->string('type_factura');
            $table->date('mes_nota_venta');
            $table->date('mes_facturado');
            $table->decimal('total_c_iva');
            $table->decimal('total_s_iva');
            //cliente
            $table->string('nro_client')->nullable();
            $table->foreign('client_id')->references('id')->on('clients');
            //Prodcuto
            $table->foreign('products_id')->references('id')->on('products');
            $table->string('codigo')->nullable();
            $table->string('cantidad')->nullable();
            $table->decimal('precio_unit_c_iva')->nullable();
            $table->decimal('precio_unit_s_iva')->nullable();
            //comisiones
            $table->string('comision_vend');
            $table->string('comision_gerencia');
            $table->decimal('comision_1_en_porcentaje', 50, 2);
            $table->decimal('comision_2_en_porcentaje', 50, 2);
            //ganancias
            $table->decimal('ganancia_unit', 50, 2);
            $table->decimal('ganancia_total_s_iva', 50, 2);
            $table->decimal('renta_costo', 50, 2);
            $table->decimal('renta_factura', 50, 2);            
            //otros
            $table->string('edo_entrega');
            $table->string('edo_cobro');
            $table->string('nro_d_nv');
            $table->string('vendedor');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales');
    }
}
