<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('item_id');
            $table->string('category_id');
            $table->string('title_item');
            $table->string('unit_price');
            $table->integer('cantidad');
            $table->string('currency_id');
            //$table->integer('order_id')->unsigned('id');
            //$table->foreign('order_id')->references('id')->on('orders')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
