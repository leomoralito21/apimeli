<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('contact');
            $table->date('date_deliver');
            $table->date('date_pro');
            $table->string('number_invoice');
            $table->string('residence');
            $table->string('phone');
            $table->string('user_ml')->nullable();
            $table->decimal('iva', 10, 2)->default(0);
            $table->decimal('total',10, 2)->default(0);
            $table->string('cuit')->nullable();
            $table->string('iibb')->nullable();
            $table->string('iibb_type')->nullable();
            $table->string('condition')->nullable();
            $table->string('seller')->nullable();
            $table->string('shipping_address')->nullable();
            $table->string('company_shipping')->nullable();
            $table->integer('seller_id')->unsigned()->nullable();
            $table->integer('client_id')->unsigned()->nullable();
            $table->foreign('seller_id')->references('id')->on('sellers');
            $table->foreign('client_id')->references('id')->on('clients');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
