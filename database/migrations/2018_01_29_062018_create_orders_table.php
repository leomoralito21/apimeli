<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('order_id');
            $table->string('status');
            $table->string('order_created');
            $table->string('date_closet');
            $table->string('expiration_date');
            $table->string('date_last_update');
            $table->string('currency_id');
            $table->decimal('total_amount');
            //shipping
            $table->string('sub_status')->nullable();
            $table->string('status_shipping')->nullable();
            $table->string('shipping_id')->nullable();
            $table->string('service_id')->nullable();
            $table->string('currency_id_shipping')->nullable();
            $table->string('shipment_type')->nullable(); 
            $table->string('sender_id')->nullable(); 
            $table->string('picking_type')->nullable(); 
            $table->string('date_created_shipping')->nullable(); 
            $table->decimal('cost_shipping')->nullable(); 
            $table->string('date_first_printed')->nullable();
            //Buyer
            $table->string('id_buyer');
            $table->string('nickname_buyer');
            $table->string('email_buyer');
            $table->string('area_code_buyer')->nullable();
            $table->string('extension_buyer')->nullable();
            $table->string('number_buyer')->nullable();
            //Datos Buyer
            $table->string('first_name_buyer');
            $table->string('last_name_buyer');
            $table->string('doc_type_buyer');
            $table->string('doc_number_buyer');
            //Seller
            $table->string('id_seller');
            $table->string('nickname_seller');
            $table->string('email_seller');
            $table->string('area_code_seller')->nullable();
            $table->string('extension_seller')->nullable();
            $table->string('number_seller')->nullable();
            //Datos seller
            $table->string('first_name_seller');
            $table->string('last_name_seller');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
