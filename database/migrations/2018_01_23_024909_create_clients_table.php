<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('code_client');
            $table->string('name_client');
            $table->string('site_id_client');
            $table->string('address');
            $table->string('code_postal');
            $table->string('cuit');
            $table->string('iva');
            $table->string('type_factura');
            $table->string('n_iibb');
            $table->string('code_inscription');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
