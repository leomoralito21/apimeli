@extends('layouts.app')

@section('content')

                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card">
                                        <div style="padding:15px" class="card-header card-header-icon" data-background-color><a href="agregar_customer.html"  rel="tooltip" title="Agregar"><i class="material-icons">assignment</i></a>
                                    </div>
                                    <div class="card-content">
                                        <h4 class="card-title">Detalles Cliente</h4>
                                        <div class="toolbar">
                                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                                        </div>
                                        <div class="material-datatables">
                                            <table id="datatables" class="table table-striped table-no-bordered table-hover table-responsive" cellspacing="0" width="100%" style="width:100%">
                                                <thead>
                                                    <tr>
                                                        <th class="disabled-sorting ">Nombre</th>
                                                        <th class="disabled-sorting ">Cod. Cliente</th>
                                                        <th class="disabled-sorting ">Direccion</th>
                                                        <th class="disabled-sorting ">Cod. Postal</th>
                                                        <th class="disabled-sorting ">Cuit</th>
                                                        <th class="disabled-sorting ">IVA</th>
                                                        <th class="disabled-sorting ">Tipo de Fact.</th>
                                                        <th class="disabled-sorting ">N° IIBB</th>
                                                        <th class="disabled-sorting ">Cod. Inscrip.</th>
                                                        <th class="disabled-sorting text-right">Editar</th>
                                                        <th class="disabled-sorting text-right">Ver Detalles</th>
                                                        <th class="disabled-sorting text-right">Eliminar</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($customers as $client)
                                                    <tr>
                                                        <td>{{ $client->name_client }}</td>
                                                        <td>{{ $client->code_client }}</td>
                                                        <td>{{ $client->address }}</td>
                                                        <td>{{ $client->code_postal }}</td>
                                                        <td>{{ $client->cuit }}</td>
                                                        <td>{{ $client->iva }}</td>
                                                        <td>{{ $client->type_factura }}</td>
                                                        <td>{{ $client->n_iibb }}</td>
                                                        <td>{{ $client->code_inscription }}</td>
                                                        <td style="width: 10%" class="td-actions text-right">
                                                        <a href="edit_customer.html" type="button" rel="tooltip" class="btn btn-success btn-simple">
                                                            <i class="material-icons">edit</i>
                                                        </a>
                                                            
                                                        </td>
                                                        <td style="width: 10%" class="td-actions text-right">
                                                            <a href="ver_customer.html" type="button" rel="tooltip" class="btn btn-success btn-simple">
                                                                <i class="material-icons"><i class="material-icons">remove_red_eye</i></i>
                                                            </a>
                                                        </td>
                                                        <td class="td-actions text-right">
                                                            <button  onclick="demo.showSwal('warning-message-and-confirmation')" type="button" rel="tooltip" class="btn btn-danger btn-simple">
                                                            <i class="material-icons">close</i>
                                                            </button>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                     
                                                    
                                                    
                                                    
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <!-- end content-->
                                </div>
                                <!--  end card  -->
                            </div>
                            <!-- end col-md-12 -->
                        </div>
                        <!-- end row -->
                    </div>

@endsection