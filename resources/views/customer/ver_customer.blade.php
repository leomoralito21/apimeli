@extends('layouts.app')

@section('content')

                        <div class="container-fluid">
                            <div class="row">
                            <div class="col-md-8 col-md-offset-2">
                                    <div class="card">
                                        <div class="card-header card-header-icon" data-background-color="rose">
                                            <i class="material-icons">shopping_cart</i>
                                        </div>
                                        <div class="card-content">
                                            <h4 class="card-title">Cliente-
                                                <small class="category">Información Detallada</small>
                                            </h4>
                                            <form>


                                                
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">Código Postal</label>
                                                            <input type="text" class="form-control" disabled>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">Localidad</label>
                                                            <input disabled type="text" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">Cuit</label>
                                                            <input disabled type="email" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">IVA</label>
                                                            <input disabled type="text" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">Tipo De Factura</label>
                                                            <input disabled type="text" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">Nº IIBB</label>
                                                            <input disabled type="text" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">Condición De Inscripción</label>
                                                            <input disabled type="text" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">Condicón De Pago</label>
                                                            <input disabled type="text" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">Teléfono</label>
                                                            <input disabled type="text" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">Vendedor</label>
                                                            <input disabled type="text" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">Observasiones</label>
                                                            <input disabled type="text" class="form-control">
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                                <div class="row">
                                                <div class="col-md-3">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">Transporte</label>
                                                            <input disabled type="text" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">Nombre De Contacto</label>
                                                            <input disabled type="text" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">Email</label>
                                                            <input disabled type="text" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                                

                                                <a href="listar_sale.html" class="btn pull-right">Volver</a>
                                                <div class="clearfix"></div>
                                            </form>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>

@endsection