@extends('layouts.app')

@section('content')


<div class="container-fluid">
                                        <div class="row">
                                            <div class="col-md-9 col-md-offset-1">
                                                <div class="card">
                                                    <div class="card-header">
                                                        <h4 class="card-title">Editar Información Del Cliente
                                                        </h4>
                                                    </div>
                                                    <div class="card-content">
                                                        <ul class="nav nav-pills nav-pills-warning">
                                                            <li class="active">
                                                                <a href="#pill1" data-toggle="tab">Información Personal</a>
                                                            </li>
                                                            <li>
                                                                <a href="#pill2" data-toggle="tab">Detalles De La Factura</a>
                                                            </li>
                                                            <li>
                                                                <a href="#pill3" data-toggle="tab">Otros Datos</a>
                                                            </li>
                                                        </ul>
                                                        <div class="tab-content">
                                                            <div class="tab-pane active" id="pill1">
                                                                <div class="row">
                                                                    <label class="col-sm-2 label-on-left">Nombre
                                                                    </label>
                                                                    <div class="col-sm-4">
                                                                        <div class="form-group label-floating is-empty">
                                                                            <label class="control-label"></label>
                                                                            <input type="text" class="form-control" value>
                                                                            <span class="help-block">*Requerido</span>
                                                                        </div>
                                                                    </div>
                                                                    <label class="col-sm-2 label-on-left">Código Del Cliente
                                                                    </label>
                                                                    <div class="col-sm-4">
                                                                        <div class="form-group label-floating is-empty">
                                                                            <label class="control-label"></label>
                                                                            <input type="text" class="form-control" value>
                                                                            <span class="help-block">*Requerido</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                 <label class="col-sm-2 label-on-left">Dirección</label>
                                                                 <div class="col-sm-4">
                                                                    <div class="form-group label-floating is-empty">
                                                                        <label class="control-label"></label>
                                                                        <input type="text" class="form-control" value>
                                                                        <span class="help-block">*Requerido</span>
                                                                    </div>
                                                                </div>
                                                                <label class="col-sm-2 label-on-left">Código Postal</label>
                                                                <div class="col-sm-4">
                                                                    <div class="form-group label-floating is-empty">
                                                                        <label class="control-label"></label>
                                                                        <input type="text" class="form-control" >
                                                                        <span class="help-block">*Requerido</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <label class="col-sm-2 label-on-left">Localidad
                                                                </label>
                                                                <div class="col-sm-4">
                                                                    <div class="form-group label-floating is-empty">
                                                                        <label class="control-label"></label>
                                                                        <input type="text" class="form-control" value>
                                                                        <span class="help-block">*Requerido</span>
                                                                    </div>
                                                                </div>
                                                                <label class="col-sm-2 label-on-left">Cuit</label>
                                                                <div class="col-sm-4">
                                                                    <div class="form-group label-floating is-empty">
                                                                        <label class="control-label"></label>
                                                                        <input type="text" class="form-control" value>
                                                                        <span class="help-block">*Requerido</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane" id="pill2">
                                                            <div class="row">
                                                                <label class="col-sm-2 label-on-left">IVA

                                                                </label>
                                                                <div class="col-sm-4">
                                                                    <div class="form-group label-floating is-empty">
                                                                        <label class="control-label"></label>
                                                                        <input type="text" class="form-control" value>
                                                                        <span class="help-block">*Requerido</span>
                                                                    </div>
                                                                </div>
                                                                <label class="col-sm-2 label-on-left">Tipo De Factura

                                                                </label>
                                                                <div class="col-sm-4">
                                                                    <div class="form-group label-floating is-empty">
                                                                        <label class="control-label"></label>
                                                                        <input type="text" class="form-control" value>
                                                                        <span class="help-block">*Requerido</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                             <label class="col-sm-2 label-on-left">Nº IIBB
                                                             </label>
                                                             <div class="col-sm-4">
                                                                <div class="form-group label-floating is-empty">
                                                                    <label class="control-label"></label>
                                                                    <input type="text" class="form-control" value>
                                                                    <span class="help-block">*Requerido</span>
                                                                </div>
                                                            </div>
                                                            <label class="col-sm-2 label-on-left">Condición De Inscripción</label>
                                                            <div class="col-sm-4">
                                                                <div class="form-group label-floating is-empty">
                                                                    <label class="control-label"></label>
                                                                    <input type="text" class="form-control" >
                                                                    <span class="help-block">*Requerido</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                         <label class="col-sm-2 label-on-left">Condicón De Pago
                                                         </label>
                                                         <div class="col-sm-4">
                                                            <div class="form-group label-floating is-empty">
                                                                <label class="control-label"></label>
                                                                <input type="text" class="form-control" value>
                                                                <span class="help-block">*Requerido</span>
                                                            </div>
                                                        </div>
                                                        <label class="col-sm-2 label-on-left">Teléfono
                                                        </label>
                                                        <div class="col-sm-4">
                                                            <div class="form-group label-floating is-empty">
                                                                <label class="control-label"></label>
                                                                <input type="text" class="form-control" >
                                                                <span class="help-block">*Requerido</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <label class="col-sm-2 label-on-left">Vendedor</label>
                                                        <div class="col-sm-2">
                                                            <div class="form-group label-floating is-empty">
                                                                <label class="control-label"></label>
                                                                <input type="text" class="form-control" value>
                                                                <span class="help-block">*Requerido</span>
                                                            </div>
                                                        </div>
                                                        <label class="col-sm-2 label-on-left">Observasiones
                                                        </label>
                                                        <div class="col-sm-6">
                                                            <div class="form-group label-floating is-empty">
                                                                <label class="control-label"></label>
                                                                <input type="text" class="form-control" value>
                                                                <span class="help-block">*Requerido</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    
                                                </div>
                                                <div class="tab-pane" id="pill3">
                                                    <div class="row">
                                                        <label class="col-sm-2 label-on-left">Transporte</label>
                                                        <div class="col-sm-4">
                                                            <div class="form-group label-floating is-empty">
                                                                <label class="control-label"></label>
                                                                <input type="text" class="form-control" value>
                                                                <span class="help-block">*Requerido</span>
                                                            </div>
                                                        </div>
                                                        <label class="col-sm-2 label-on-left">Nombre De Contacto
                                                        </label>
                                                        <div class="col-sm-4">
                                                            <div class="form-group label-floating is-empty">
                                                                <label class="control-label"></label>
                                                                <input type="text" class="form-control" value>
                                                                <span class="help-block">*Requerido</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                     
                                                       <label class="col-sm-2 label-on-left">Email</label>
                                                       <div class="col-sm-4">
                                                        <div class="form-group label-floating is-empty">
                                                            <label class="control-label"></label>
                                                            <input type="text" class="form-control" value>
                                                            <span class="help-block">*Requerido</span>
                                                        </div>
                                                    </div>
                                                    <label class="col-sm-2 label-on-left">Domicilio De Entrega</label>
                                                    <div class="col-sm-4">
                                                        <div class="form-group label-floating is-empty">
                                                            <label class="control-label"></label>
                                                            <input type="text" class="form-control" value>
                                                            <span class="help-block">*Requerido</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                  
                                                   
                                                </div>
                                            </div>
                                            
                                            <div class="card-content footer text-center">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="col-md-4 col-md-offset-1">
                                                            <a href="" class="btn">Aceptar</a>
                                                        </div>
                                                        <div class="col-md-4 col-md-offset-1">
                                                            <a href="listar_customer.html" class="btn">Cancelar</a>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        
                    </div>

@endsection