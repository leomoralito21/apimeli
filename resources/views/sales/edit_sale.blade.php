@extends('layouts.app')

@section('content')

<div class="container-fluid">
                                <div class="row">
                                    <div class="col-md-9 col-md-offset-1">
                                        <div class="card">
                                            <div class="card-header">
                                                <h4 class="card-title">Editar Detalles De Venta
                                                </h4>
                                            </div>
                                            <div class="card-content">
                                                <ul class="nav nav-pills nav-pills-warning">
                                                    <li class="active">
                                                        <a href="#pill1" data-toggle="tab">Factura</a>
                                                    </li>
                                                    <li>
                                                        <a href="#pill2" data-toggle="tab">Cliente</a>
                                                    </li>
                                                    <li>
                                                        <a href="#pill3" data-toggle="tab">Producto</a>
                                                    </li>
                                                    <li>
                                                        <a href="#pill4" data-toggle="tab">Comisión</a>
                                                    </li>
                                                    <li>
                                                        <a href="#pill5" data-toggle="tab">Ganancias</a>
                                                    </li>
                                                    <li>
                                                        <a href="#pill6" data-toggle="tab">Otros</a>
                                                    </li>
                                                </ul>
                                                <div class="tab-content">
                                                    <div class="tab-pane active" id="pill1">
                                                        <div class="row">
                                                            <label class="col-sm-2 label-on-left">Nº Factura</label>
                                                            <div class="col-sm-4">
                                                                <div class="form-group label-floating is-empty">
                                                                    <label class="control-label"></label>
                                                                    <input type="text" class="form-control" value>
                                                                    <span class="help-block">*Requerido</span>
                                                                </div>
                                                            </div>
                                                            <label class="col-sm-2 label-on-left">Tipo De Factura</label>
                                                            <div class="col-sm-4">
                                                                <div class="form-group label-floating is-empty">
                                                                    <label class="control-label"></label>
                                                                    <input type="text" class="form-control" value>
                                                                    <span class="help-block">*Requerido</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                           <label class="col-sm-2 label-on-left">Mes Nota Venta</label>
                                                            <div class="col-sm-4">
                                                                <div class="form-group label-floating is-empty">
                                                                    <label class="control-label"></label>
                                                                    <input type="text" class="form-control" value=Enero>
                                                                    <span class="help-block">*Requerido</span>
                                                                </div>
                                                            </div>
                                                             <label class="col-sm-2 label-on-left">Mes Facturado</label>
                                                            <div class="col-sm-4">
                                                                <div class="form-group label-floating is-empty">
                                                                    <label class="control-label"></label>
                                                                    <input type="text" class="form-control" value=Enero>
                                                                    <span class="help-block">*Requerido</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <label class="col-sm-2 label-on-left">Total C/IVA</label>
                                                            <div class="col-sm-4">
                                                                <div class="form-group label-floating is-empty">
                                                                    <label class="control-label"></label>
                                                                    <input type="text" class="form-control" value>
                                                                    <span class="help-block">*Requerido</span>
                                                                </div>
                                                            </div>
                                                            <label class="col-sm-2 label-on-left">Total S/IVA</label>
                                                            <div class="col-sm-4">
                                                                <div class="form-group label-floating is-empty">
                                                                    <label class="control-label"></label>
                                                                    <input type="text" class="form-control" value>
                                                                    <span class="help-block">*Requerido</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane" id="pill2">
                                                        <div class="row">
                                                            <label class="col-sm-2 label-on-left">Nombre Del Clinte</label>
                                                            <div class="col-sm-4">
                                                                <select class="selectpicker" data-style="select-with-transition"  data-size="7">
                                                                
                                                                <option value="2">AGRO CHACO S.R.L. </option>
                                                                <option value="3">Bucharest</option>
                                                           
                                                               </select>
                                                            </div>
                                                            <label class="col-sm-2 label-on-left">Nº Del Cliente</label>
                                                            <div class="col-sm-4">
                                                                <div class="form-group label-floating is-empty">
                                                                    <label class="control-label"></label>
                                                                    <input type="text" class="form-control" value>
                                                                    <span class="help-block">*Requerido</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                       
                                                        
                                                    </div>
                                                    <div class="tab-pane" id="pill3">
                                                        <div class="row">
                                                            <label class="col-sm-2 label-on-left">Descripcion</label>
                                                            <div class="col-sm-10">
                                                                <div class="form-group label-floating is-empty">
                                                                    <label class="control-label"></label>
                                                                    <input type="text" class="form-control" value>
                                                                    <span class="help-block">*Requerido</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                           <label class="col-sm-2 label-on-left">Codigo</label>
                                                            <div class="col-sm-4">
                                                                <div class="form-group label-floating is-empty">
                                                                    <label class="control-label">AS456</label>
                                                                    <input type="text" class="form-control" value>
                                                                    <span class="help-block">*Requerido</span>
                                                                </div>
                                                            </div>
                                                             <label class="col-sm-2 label-on-left">Cantidad</label>
                                                            <div class="col-sm-4">
                                                                <div class="form-group label-floating is-empty">
                                                                    <label class="control-label">55</label>
                                                                    <input type="text" class="form-control" value>
                                                                    <span class="help-block">*Requerido</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                           <label class="col-sm-2 label-on-left">Precio Unitario C/IVA</label>
                                                            <div class="col-sm-4">
                                                                <div class="form-group label-floating is-empty">
                                                                    <label class="control-label">$3456</label>
                                                                    <input type="text" class="form-control" value>
                                                                    <span class="help-block">*Requerido</span>
                                                                </div>
                                                            </div>
                                                             <label class="col-sm-2 label-on-left">Precio Unitario S/IVA</label>
                                                            <div class="col-sm-4">
                                                                <div class="form-group label-floating is-empty">
                                                                    <label class="control-label">$2349</label>
                                                                    <input type="text" class="form-control" value>
                                                                    <span class="help-block">*Requerido</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                     <div class="tab-pane" id="pill4">
                                                        <div class="row">
                                                           <label class="col-sm-2 label-on-left">Edo. Comision Vendedores</label>
                                                            <div class="col-sm-4">
                                                                <div class="form-group label-floating is-empty">
                                                                    <label class="control-label"></label>
                                                                    <input type="text" class="form-control" value>
                                                                    <span class="help-block">*Requerido</span>
                                                                </div>
                                                            </div>
                                                             <label class="col-sm-2 label-on-left">Edo. Comision Gerencial</label>
                                                            <div class="col-sm-4">
                                                                <div class="form-group label-floating is-empty">
                                                                    <label class="control-label"></label>
                                                                    <input type="text" class="form-control" value>
                                                                    <span class="help-block">*Requerido</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                           <label class="col-sm-2 label-on-left">Comisión 1 (En %)</label>
                                                            <div class="col-sm-4">
                                                                <div class="form-group label-floating is-empty">
                                                                    <label class="control-label">$3456</label>
                                                                    <input type="text" class="form-control" value>
                                                                    <span class="help-block">*Requerido</span>
                                                                </div>
                                                            </div>
                                                             <label class="col-sm-2 label-on-left">Comisión 2 (En %)</label>
                                                            <div class="col-sm-4">
                                                                <div class="form-group label-floating is-empty">
                                                                    <label class="control-label">$2349</label>
                                                                    <input type="text" class="form-control" value>
                                                                    <span class="help-block">*Requerido</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane" id="pill5">
                                                        <div class="row">
                                                           <label class="col-sm-2 label-on-left">Ganacia Unitaria</label>
                                                            <div class="col-sm-4">
                                                                <div class="form-group label-floating is-empty">
                                                                    <label class="control-label">$3456</label>
                                                                    <input type="text" class="form-control" value>
                                                                    <span class="help-block">*Requerido</span>
                                                                </div>
                                                            </div>
                                                             <label class="col-sm-2 label-on-left">Ganacia Total S/IVA</label>
                                                            <div class="col-sm-4">
                                                                <div class="form-group label-floating is-empty">
                                                                    <label class="control-label">$2349</label>
                                                                    <input type="text" class="form-control" value>
                                                                    <span class="help-block">*Requerido</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                           <label class="col-sm-2 label-on-left">Renta/Fact</label>
                                                            <div class="col-sm-4">
                                                                <div class="form-group label-floating is-empty">
                                                                    <label class="control-label">$3456</label>
                                                                    <input type="text" class="form-control" value>
                                                                    <span class="help-block">*Requerido</span>
                                                                </div>
                                                            </div>
                                                             <label class="col-sm-2 label-on-left">Renta/Costo</label>
                                                            <div class="col-sm-4">
                                                                <div class="form-group label-floating is-empty">
                                                                    <label class="control-label">$2349</label>
                                                                    <input type="text" class="form-control" value>
                                                                    <span class="help-block">*Requerido</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane" id="pill6">
                                                        <div class="row">
                                                           <label class="col-sm-2 label-on-left">Estado De Entrega</label>
                                                            <div class="col-sm-4">
                                                                <div class="form-group label-floating is-empty">
                                                                    <label class="control-label">Entregado</label>
                                                                    <input type="text" class="form-control" value>
                                                                    <span class="help-block">*Requerido</span>
                                                                </div>
                                                            </div>
                                                             <label class="col-sm-2 label-on-left">Cobro</label>
                                                            <div class="col-sm-4">
                                                                <div class="form-group label-floating is-empty">
                                                                    <label class="control-label">$2349</label>
                                                                    <input type="text" class="form-control" value>
                                                                    <span class="help-block">*Requerido</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                           <label class="col-sm-2 label-on-left">Nº de NV</label>
                                                            <div class="col-sm-4">
                                                                <div class="form-group label-floating is-empty">
                                                                    <label class="control-label">Entregado</label>
                                                                    <input type="text" class="form-control" value>
                                                                    <span class="help-block">*Requerido</span>
                                                                </div>
                                                            </div>
                                                             <label class="col-sm-2 label-on-left">Vendedor</label>
                                                            <div class="col-sm-4">
                                                                <div class="form-group label-floating is-empty">
                                                                    <label class="control-label">$2349</label>
                                                                    <input type="text" class="form-control" value>
                                                                    <span class="help-block">*Requerido</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                     <div class="card-content footer text-center">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="col-md-4 col-md-offset-1">
                                                                <a href="" class="btn">Aceptar</a>
                                                            </div>
                                                            <div class="col-md-4 col-md-offset-1">
                                                                <a href="listar_sale.html" class="btn">Cancelar</a>
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                
                                </div>
                                
                            </div>

@endsection