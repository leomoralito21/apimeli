@extends('layouts.app')

@section('content')

                                <div class="container-fluid">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="card card-chart">
                                                    <div class="card-header" style="background:#2fc556" data-background-color="rose" data-header-animation="true">
                                                        <div class="ct-chart" id="websiteViewsChart"></div>
                                                    </div>
                                                    <div class="card-content">
                                                        <div class="card-actions">
                                                            <button type="button" class="btn btn-info btn-simple" rel="tooltip" data-placement="bottom" title="Refresh">
                                                                <i class="material-icons">refresh</i>
                                                            </button>
                                                            <button type="button" class="btn btn-default btn-simple" rel="tooltip" data-placement="bottom" title="Change Date">
                                                                <i class="material-icons">edit</i>
                                                            </button>
                                                        </div>
                                                        <h4 class="card-title">Ventas</h4>
                                                        <p class="category">Last Campaign Performance</p>
                                                    </div>
                                                    <div class="card-footer">
                                                        <div class="stats">
                                                            <i class="material-icons">access_time</i> campaign sent 2 days ago
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="card card-chart">
                                                    <div class="card-header" data-background-color="green" style="background:#ffa726;" data-header-animation="true">
                                                        <div class="ct-chart" id="dailySalesChart"></div>
                                                    </div>
                                                    <div class="card-content">
                                                        <div class="card-actions">
                                                            <button type="button" class="btn btn-danger btn-simple fix-broken-card">
                                                                <i class="material-icons">build</i> Fix Header!
                                                            </button>
                                                            <button type="button" class="btn btn-info btn-simple" rel="tooltip" data-placement="bottom" title="Refresh">
                                                                <i class="material-icons">refresh</i>
                                                            </button>
                                                            <button type="button" class="btn btn-default btn-simple" rel="tooltip" data-placement="bottom" title="Change Date">
                                                                <i class="material-icons">edit</i>
                                                            </button>
                                                        </div>
                                                        <h4 class="card-title">Ventas Diarias</h4>
                                                        <p class="category">
                                                            <span class="text-success"><i class="fa fa-long-arrow-up"></i> 55% </span> increase in today sales.</p>
                                                        </div>
                                                        <div class="card-footer">
                                                            <div class="stats">
                                                                <i class="material-icons">access_time</i> updated 4 minutes ago
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="card card-chart">
                                                        <div class="card-header" data-background-color="blue" style="background:#4770ce" data-header-animation="true">
                                                            <div class="ct-chart" id="completedTasksChart"></div>
                                                        </div>
                                                        <div class="card-content">
                                                            <div class="card-actions">
                                                                <button type="button" class="btn btn-danger btn-simple fix-broken-card">
                                                                    <i class="material-icons">build</i> Fix Header!
                                                                </button>
                                                                <button type="button" class="btn btn-info btn-simple" rel="tooltip" data-placement="bottom" title="Refresh">
                                                                    <i class="material-icons">refresh</i>
                                                                </button>
                                                                <button type="button" class="btn btn-default btn-simple" rel="tooltip" data-placement="bottom" title="Change Date">
                                                                    <i class="material-icons">edit</i>
                                                                </button>
                                                            </div>
                                                            <h4 class="card-title">Ganacias</h4>
                                                            <p class="category">Last Campaign Performance</p>
                                                        </div>
                                                        <div class="card-footer">
                                                            <div class="stats">
                                                                <i class="material-icons">access_time</i> campaign sent 2 days ago
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-3 col-md-6 col-sm-6">
                                                <div class="card card-stats">
                                                    <div class="card-header" data-background-color style="background:#7986cb">
                                                        <i class="material-icons">swap_vert</i>
                                                    </div>
                                                    <div class="card-content">
                                                        <p class="category">Costos</p>
                                                        <h3 class="card-title">75.521</h3>
                                                    </div>
                                                    <div class="card-footer">
                                                        <div class="stats">
                                                            <i class="material-icons">local_offer</i> Tracked from Google Analytics
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-6 col-sm-6">
                                                <div class="card card-stats">
                                                    <div class="card-header" data-background-color="rose">
                                                        <i class="material-icons">equalizer</i>
                                                    </div>
                                                    <div class="card-content">
                                                        <p class="category">Costos</p>
                                                        <h3 class="card-title">75.521</h3>
                                                    </div>
                                                    <div class="card-footer">
                                                        <div class="stats">
                                                            <i class="material-icons">local_offer</i> Tracked from Google Analytics
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="col-lg-3 col-md-6 col-sm-6">
                                                <div class="card card-stats">
                                                    <div class="card-header header_card" data-background-color="rose" style="background:red">
                                                     <i class="material-icons">update</i> 
                                                 </div>
                                                 <div class="card-content">
                                                    <p class="category">Total Costos</p>
                                                    <h3 class="card-title">-245</h3>
                                                </div>
                                                <div class="card-footer">
                                                    <div class="stats">
                                                        <i class="material-icons">update</i> Just Updated
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-6 col-sm-6">
                                            <div class="card card-stats">
                                                <div class="card-header" data-background-color style="background:blue">
                                                 <i class="material-icons">assessment</i> 
                                             </div>
                                             <div class="card-content">
                                                <p class="category">Total Costos</p>
                                                <h3 class="card-title">-245</h3>
                                            </div>
                                            <div class="card-footer">
                                                <div class="stats">
                                                    <i class="material-icons">update</i> Just Updated
                                                </div>
                                            </div>
                                        </div>
                                    </div>

@endsection