@extends('layouts.app')

@section('content')

<div class="container-fluid">
                            <div class="row">
                            <div class="col-md-8 col-md-offset-2">
                                    <div class="card">
                                        <div class="card-header card-header-icon" data-background-color="rose">
                                            <i class="material-icons">view_list</i>
                                        </div>
                                        <div class="card-content">
                                            <h4 class="card-title">Producto-
                                                <small class="category">Información Detallada</small>
                                            </h4>
                                            <form>
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">Descripción</label>
                                                            <input type="text" class="form-control" disabled>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">Cantidad</label>
                                                            <input disabled type="text" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">Código</label>
                                                            <input disabled type="text" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">Precio Unitario C/IVA</label>
                                                            <input type="text" class="form-control" disabled>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">Precio Unitario S/IVA</label>
                                                            <input disabled type="text" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">Costo Unitario</label>
                                                            <input disabled type="email" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                               

                                                <a href="listar_producto.html" class="btn pull-right">Volver</a>
                                                <div class="clearfix"></div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

@endsection