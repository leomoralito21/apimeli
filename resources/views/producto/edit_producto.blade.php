@extends('layouts.app')

@section('content')

                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-md-9 col-md-offset-1">
                                        <div class="card">
                                            <div class="card-header">
                                                <h4 class="card-title">Editar Producto
                                                </h4>
                                            </div>
                                            <div class="card-content">
                                                <ul class="nav nav-pills nav-pills-warning">
                                                    <li class="active">
                                                        <a href="#pill1" data-toggle="tab">Factura</a>
                                                    </li>
                                                    
                                                </ul>
                                                <div class="tab-content">
                                                    <div class="tab-pane active" id="pill1">
                                                        <div class="row">
                                                            <label class="col-sm-2 label-on-left">Descripcion</label>
                                                            <div class="col-sm-4">
                                                                <div class="form-group label-floating is-empty">
                                                                    <label class="control-label"></label>
                                                                    <input type="text" class="form-control" value>
                                                                    <span class="help-block">*Requerido</span>
                                                                </div>
                                                            </div>
                                                            <label class="col-sm-2 label-on-left">Codigo</label>
                                                            <div class="col-sm-4">
                                                                <div class="form-group label-floating is-empty">
                                                                    <label class="control-label"></label>
                                                                    <input type="text" class="form-control" value>
                                                                    <span class="help-block">*Requerido</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                         <label class="col-sm-2 label-on-left">Cantidad</label>
                                                         <div class="col-sm-4">
                                                            <div class="form-group label-floating is-empty">
                                                                <label class="control-label"></label>
                                                                <input type="text" class="form-control" value>
                                                                <span class="help-block">*Requerido</span>
                                                            </div>
                                                        </div>
                                                        <label class="col-sm-2 label-on-left">Precio Unitario C/IVA</label>
                                                        <div class="col-sm-4">
                                                            <div class="form-group label-floating is-empty">
                                                                <label class="control-label"></label>
                                                                <input type="text" class="form-control" value>
                                                                <span class="help-block">*Requerido</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <label class="col-sm-2 label-on-left">Precio Unitario S/IVA</label>
                                                        <div class="col-sm-4">
                                                            <div class="form-group label-floating is-empty">
                                                                <label class="control-label"></label>
                                                                <input type="text" class="form-control" value>
                                                                <span class="help-block">*Requerido</span>
                                                            </div>
                                                        </div>
                                                        <label class="col-sm-2 label-on-left">Costo Unitario</label>
                                                        <div class="col-sm-4">
                                                            <div class="form-group label-floating is-empty">
                                                                <label class="control-label"></label>
                                                                <input type="text" class="form-control" value>
                                                                <span class="help-block">*Requerido</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="card-content footer text-center">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="col-md-4 col-md-offset-1">
                                                                <a href="" class="btn">Aceptar</a>
                                                            </div>
                                                            <div class="col-md-4 col-md-offset-1">
                                                                <a href="listar_producto.html" class="btn">Cancelar</a>
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>

                        </div>

@endsection