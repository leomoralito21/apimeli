@extends('layouts.app')

@section('content')

                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="card">
                                                    <div style="padding:15px" class="card-header card-header-icon" data-background-color><a href="agregar_sale.html"  rel="tooltip" title="Agregar"><i class="material-icons">assignment</i></a>
                                                    </div>
                                                    <div class="card-content">
                                                        <h4 class="card-title">Lista De Productos</h4>
                                                        <div class="toolbar">
                                                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                                                        </div>
                                                        <div class="material-datatables">
                                                            <table id="datatables" class="table table-striped table-no-bordered table-hover table-responsive" cellspacing="0" width="100%" style="width:100%">
                                                                <thead>
                                                                    
                                                                    <tr>
                                                                        <th class="disabled-sorting ">Descripción</th>
                                                                        <th class="disabled-sorting ">Codigo</th>
                                                                        <th class="disabled-sorting ">Cantidad</th>
                                                                        <th class="disabled-sorting ">Precio Unitario C/IVA</th>
                                                                        <th class="disabled-sorting ">Precio Unitario S/IVA</th>
                                                                        <th class="disabled-sorting ">Costo Unitario</th>
                                                                        <th class="disabled-sorting text-right">Editar</th>
                                                                        <th class="disabled-sorting text-right">Ver Detalles</th>
                                                                        <th class="disabled-sorting text-right">Eliminar</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    @foreach ($products as product)                                                      
                                                                    <tr>
                                                                        <td>R10016150</td>
                                                                        <td>Tejido Romboidal Perimtral</td>
                                                                        <td>Internet Final</td>
                                                                        <td>9998</td>
                                                                        <td>$510</td>
                                                                        <td>$461,98</td>
                                                                        <td style="width: 10%" class="td-actions text-right">
                                                                            <a href="edit_producto.html" type="button" rel="tooltip" class="btn btn-success btn-simple">
                                                                                <i class="material-icons">edit</i>
                                                                            </a>
                                                                            
                                                                        </td>
                                                                        <td style="width: 10%" class="td-actions text-right">
                                                                            <a href="ver_producto.html" type="button" rel="tooltip" class="btn btn-success btn-simple">
                                                                                <i class="material-icons"><i class="material-icons">remove_red_eye</i></i>
                                                                            </a>
                                                                        </td>
                                                                        <td class="td-actions text-right">
                                                                            <button  onclick="demo.showSwal('warning-message-and-confirmation')" type="button" rel="tooltip" class="btn btn-danger btn-simple">
                                                                                <i class="material-icons">close</i>
                                                                            </button>
                                                                        </td>
                                                                    </tr>
                                                                    @endforeach
                                                                    <tr>
                                                                        <td>R10016150</td>
                                                                        <td>Tejido Romboidal Perimtral</td>
                                                                        <td>Internet Final</td>
                                                                        <td>9998</td>
                                                                        <td>$510</td>
                                                                        <td>$461,98</td>
                                                                        <td style="width: 10%" class="td-actions text-right">
                                                                            <a href="edit_producto.html" type="button" rel="tooltip" class="btn btn-success btn-simple">
                                                                                <i class="material-icons">edit</i>
                                                                            </a>
                                                                            
                                                                        </td>
                                                                        <td style="width: 10%" class="td-actions text-right">
                                                                            <a href="ver_producto.html" type="button" rel="tooltip" class="btn btn-success btn-simple">
                                                                                <i class="material-icons"><i class="material-icons">remove_red_eye</i></i>
                                                                            </a>
                                                                        </td>
                                                                        <td class="td-actions text-right">
                                                                            <button  onclick="demo.showSwal('warning-message-and-confirmation')" type="button" rel="tooltip" class="btn btn-danger btn-simple">
                                                                                <i class="material-icons">close</i>
                                                                            </button>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>R10016150</td>
                                                                        <td>Tejido Romboidal Perimtral</td>
                                                                        <td>Internet Final</td>
                                                                        <td>9998</td>
                                                                        <td>$510</td>
                                                                        <td>$461,98</td>
                                                                        <td style="width: 10%" class="td-actions text-right">
                                                                            <a href="edit_producto.html" type="button" rel="tooltip" class="btn btn-success btn-simple">
                                                                                <i class="material-icons">edit</i>
                                                                            </a>
                                                                            
                                                                        </td>
                                                                        <td style="width: 10%" class="td-actions text-right">
                                                                            <a href="ver_sale.html" type="button" rel="tooltip" class="btn btn-success btn-simple">
                                                                                <i class="material-icons"><i class="material-icons">remove_red_eye</i></i>
                                                                            </a>
                                                                        </td>
                                                                        <td class="td-actions text-right">
                                                                            <button  onclick="demo.showSwal('warning-message-and-confirmation')" type="button" rel="tooltip" class="btn btn-danger btn-simple">
                                                                                <i class="material-icons">close</i>
                                                                            </button>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>R10016150</td>
                                                                        <td>Tejido Romboidal Perimtral</td>
                                                                        <td>Internet Final</td>
                                                                        <td>9998</td>
                                                                        <td>$510</td>
                                                                        <td>$461,98</td>
                                                                        <td style="width: 10%" class="td-actions text-right">
                                                                            <a href="edit_producto.html" type="button" rel="tooltip" class="btn btn-success btn-simple">
                                                                                <i class="material-icons">edit</i>
                                                                            </a>
                                                                            
                                                                        </td>
                                                                        <td style="width: 10%" class="td-actions text-right">
                                                                            <a href="ver_producto.html" type="button" rel="tooltip" class="btn btn-success btn-simple">
                                                                                <i class="material-icons"><i class="material-icons">remove_red_eye</i></i>
                                                                            </a>
                                                                        </td>
                                                                        <td class="td-actions text-right">
                                                                            <button  onclick="demo.showSwal('warning-message-and-confirmation')" type="button" rel="tooltip" class="btn btn-danger btn-simple">
                                                                                <i class="material-icons">close</i>
                                                                            </button>
                                                                        </td>
                                                                    </tr>
                                                                    

                                                                    
                                                                    
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <!-- end content-->
                                                </div>
                                                <!--  end card  -->
                                            </div>
                                            <!-- end col-md-12 -->
                                        </div>
                                        <!-- end row -->
                                    </div>

@endsection