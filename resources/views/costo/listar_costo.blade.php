@extends('layouts.app')

@section('content')

<div class="container-fluid">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card">
                                        <div style="padding:15px" class="card-header card-header-icon" data-background-color><a href="agregar_costo.html"  rel="tooltip" title="Agregar"><i class="material-icons">image</i></a>
                                    </div>
                                    <div class="card-content">
                                        <h4 class="card-title">Lista De Costo</h4>
                                        <div class="toolbar">
                                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                                        </div>
                                        <div class="material-datatables">
                                            <table id="datatables" class="table table-striped table-no-bordered table-hover table-responsive" cellspacing="0" width="100%" style="width:100%">
                                                <thead>
                                                    <tr>
                                                        <th class="disabled-sorting ">Código</th>
                                                        <th  class="disabled-sorting ">Producto</th>
                                                        <th class="disabled-sorting ">Costo</th>
                                                        <th class="disabled-sorting text-right">Editar</th>
                                                        <th class="disabled-sorting text-right">Ver Detalles</th>
                                                        <th class="disabled-sorting text-right">Eliminar</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    @forelse ($costos as $costo)                                                    
                                                    <tr>
                                                        <td>{{$costo['code']}}</td>
                                                        <td>{{ $costo['description'] }}</td>
                                                        <td>{{ $costo['venta']}}</td>
                                                        <td style="width: 10%" class="td-actions text-right">
                                                        <a href="edit_costo.html" type="button" rel="tooltip" class="btn btn-success btn-simple">
                                                            <i class="material-icons">edit</i>
                                                        </a>
                                                            
                                                        </td>
                                                        <td style="width: 10%" class="td-actions text-right">
                                                            <a href="ver_costo.html" type="button" rel="tooltip" class="btn btn-success btn-simple">
                                                                <i class="material-icons"><i class="material-icons">remove_red_eye</i></i>
                                                            </a>
                                                        </td>
                                                        <td class="td-actions text-right">
                                                            <button  onclick="demo.showSwal('warning-message-and-confirmation')" type="button" rel="tooltip" class="btn btn-danger btn-simple">
                                                            <i class="material-icons">close</i>
                                                            </button>
                                                        </td>
                                                    </tr>
                                                    @empty
                                                     <tr><td colspan="11">No existen registros</td></tr>
                                                    @endforelse
                                                    
                                                    
                                                    
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <!-- end content-->
                                </div>
                                <!--  end card  -->
                            </div>
                            <!-- end col-md-12 -->
                        </div>
                        <!-- end row -->
                    </div>

@endsection