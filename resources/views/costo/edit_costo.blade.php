@extends('layouts.app')

@section('content')


							<div class="container-fluid">
                                <div class="row">
                                    <div class="col-md-9 col-md-offset-1">
                                        <form action="{{route('costo.update', $costo['id'])}}" method="POST" autocomplete="on">
                                        <div class="card">
                                            <div class="card-header">
                                                <h4 class="card-title">Editar Costo
                                                </h4>
                                            </div>
                                            <div class="card-content">
                                                <ul class="nav nav-pills nav-pills-warning">
                                                    <li class="active">
                                                        <a href="#pill1" data-toggle="tab">Información Detallada</a>
                                                    </li>                                                    
                                                </ul>
                                                <div class="tab-content">
                                                    <div class="tab-pane active" id="pill1">
                                                        <div class="row">
                                                            <label class="col-sm-2 label-on-left">Código</label>
                                                            <div class="col-sm-4">
                                                                <div class="form-group label-floating is-empty">
                                                                    <label class="control-label"></label>
                                                                    <input name="code" required="required" value="$costo['code']" placeholder="Codigo" type="text" class="form-control" value>
                                                                    <span class="help-block">*Requerido</span>
                                                                </div>
                                                            </div>
                                                            <label class="col-sm-2 label-on-left">Descripción

                                                            </label>
                                                            <div class="col-sm-4">
                                                                <div class="form-group label-floating is-empty">
                                                                    <label class="control-label"></label>
                                                                    <input name="description" required="required" placeholder="Descripcion" type="text" class="form-control" value="$costo['description']">
                                                                    <span class="help-block">*Requerido</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                         <label class="col-sm-2 label-on-left">Venta</label>
                                                         <div class="col-sm-4">
                                                            <div class="form-group label-floating is-empty">
                                                                <label class="control-label"></label>
                                                                <input name="venta" placeholder="Venta" required="required" type="text" class="form-control" value="$costo['venta']">
                                                                <span class="help-block">*Requerido</span>
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                    
                                                <div class="card-content footer text-center">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="col-md-4 col-md-offset-1">
                                                                <a href="" class="btn">Aceptar</a>
                                                            </div>
                                                            <div class="col-md-4 col-md-offset-1">
                                                                <a href="listar_costo.html" class="btn">Cancelar</a>
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                                
                            </div>

                        </div>


@endsection