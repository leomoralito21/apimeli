<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <link rel="apple-touch-icon" sizes="76x76" href="{{ asset ('img/apple-icon.png') }}" />
        <link rel="icon" type="image/png" href="{{ asset ('img/favicon.png') }}" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <title>Material Dashboard</title>
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <meta name="viewport" content="width=device-width" />
        <!-- Bootstrap core CSS     -->
        <link href="{{ asset ('css/bootstrap.min.css') }}" rel="stylesheet" />
        <!--  Material Dashboard CSS    -->
        <link href="{{ asset ('css/material-dashboard.css') }}" rel="stylesheet" />
        <!--  CSS for Demo Purpose, don't include it in your project     -->
        <link href="{{ asset ('css/demo.css') }}" rel="stylesheet" />
        <!--     Fonts and icons     -->
        <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons" />
    </head>
    <body>
        <nav class="navbar navbar-primary navbar-transparent navbar-absolute">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    </button>
                </div>
                
            </nav>
            <div class="wrapper wrapper-full-page">
                <div class="full-page login-page" filter-color="black" data-image="{{ asset ('img/sidebar-4.jpg') }}">
                    <!--   you can change the color of the filter page using: data-color="blue | purple | green | orange | red | rose " -->
                    <div class="content">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3">
                                    <form action="{{route('users.store')}}" method="post" autocomplete="on">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <div class="card card-login card-hidden">
                                            <div class="card-header text-center" data-background-color>
                                                <h4 class="card-title">Registro</h4>
                                            </div>                                            
                                            <div class="card-content">
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="material-icons">face</i>
                                                    </span>
                                                    <div class="form-group label-floating">
                                                        <label class="control-label">Nombre</label>
                                                        <input type="text" name="name" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="material-icons">email</i>
                                                    </span>
                                                    <div class="form-group label-floating">
                                                        <label class="control-label">Correo</label>
                                                        <input type="email" name="email" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="material-icons">lock_outline</i>
                                                    </span>
                                                    <div class="form-group label-floating">
                                                        <label class="control-label">Contraseña</label>
                                                        <input type="password" name="password" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="material-icons">lock_outline</i>
                                                    </span>
                                                    <div class="form-group label-floating">
                                                        <label class="control-label">Repetir Contraseña</label>
                                                        <input type="password" name="password" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card-content footer text-center">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="col-md-4 col-md-offset-1">
                                                            <button type="submit" class="btn">Guardar</button>
                                                        </div>
                                                        <div class="col-md-4 col-md-offset-1">
                                                            <button type="submit" class="btn">Cancelar</button>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <footer class="footer">
                            <div class="container">
                                
                            </div>
                        </footer>
                    </div>
                </div>
            </body>
            <!--   Core JS Files   -->
            <script src="{{ asset ('js/jquery-3.1.1.min.js') }}" type="text/javascript"></script>
            <script src="{{ asset ('js/jquery-ui.min.js') }}" type="text/javascript"></script>
            <script src="{{ asset ('js/bootstrap.min.js') }}" type="text/javascript"></script>
            <script src="{{ asset ('js/material.min.js') }}" type="text/javascript"></script>
            <script src="{{ asset ('js/perfect-scrollbar.jquery.min.js') }}" type="text/javascript"></script>
            <!-- Forms Validations Plugin -->
            <script src="{{ asset ('js/jquery.validate.min.js') }}"></script>
            <!--  Plugin for Date Time Picker and Full Calendar Plugin-->
            <script src="{{ asset ('js/moment.min.js') }}"></script>
            <!--  Charts Plugin -->
            <script src="{{ asset ('js/chartist.min.js') }}"></script>
            <!--  Plugin for the Wizard -->
            <script src="{{ asset ('js/jquery.bootstrap-wizard.js') }}"></script>
            <!--  Notifications Plugin    -->
            <script src="{{ asset ('js/bootstrap-notify.js') }}"></script>
            <!-- DateTimePicker Plugin -->
            <script src="{{ asset ('js/bootstrap-datetimepicker.js') }}"></script>
            <!-- Vector Map plugin -->
            <script src="{{ asset ('js/jquery-jvectormap.js') }}"></script>
            <!-- Sliders Plugin -->
            <script src="{{ asset ('js/nouislider.min.js') }}"></script>
            <!--  Google Maps Plugin    -->
            <script src="https://maps.googleapis.com/maps/api/js"></script>
            <!-- Select Plugin -->
            <script src="{{ asset ('js/jquery.select-bootstrap.js') }}"></script>
            <!--  DataTables.net Plugin    -->
            <script src="{{ asset ('js/jquery.datatables.js') }}"></script>
            <!-- Sweet Alert 2 plugin -->
            <script src="{{ asset ('js/sweetalert2.js') }}"></script>
            <!--    Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
            <script src="{{ asset ('js/jasny-bootstrap.min.js') }}"></script>
            <!--  Full Calendar Plugin    -->
            <script src="{{ asset ('js/fullcalendar.min.js') }}"></script>
            <!-- TagsInput Plugin -->
            <script src="{{ asset ('js/jquery.tagsinput.js') }}"></script>
            <!-- Material Dashboard javascript methods -->
            <script src="{{ asset ('js/material-dashboard.js') }}"></script>
            <!-- Material Dashboard DEMO methods, don't include it in your project! -->
            <script src="{{ asset ('js/demo.js') }}"></script>
            <script type="text/javascript">
            $().ready(function() {
            demo.checkFullPageBackgroundImage();
            setTimeout(function() {
            // after 1000 ms we add the class animated to the login/register card
            $('.card').removeClass('card-hidden');
            }, 700)
            });
            </script>
        </html>