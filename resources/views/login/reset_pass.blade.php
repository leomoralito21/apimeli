@extends('layouts.app')
@section('content')

                <div class="container">
                    <div class="row">
                        <div class="col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3">
                            <form method="#" action="#">
                                <div class="card card-login card-hidden">
                                    <div class="card-header text-center" data-background-color>
                                        <h4 class="card-title">Restaurar Contraseña</h4>
                                    </div>
                                    
                                    <div class="card-content">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">email</i>
                                            </span>
                                            <div class="form-group label-floating">
                                                <label class="control-label">Email address</label>
                                                <input type="email" class="form-control">
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <div class="card-content footer text-center" aling="center">
                                    <button type="submit" class="btn">Enviar</button>
                                    </div>

                                </div>
                            </form>
                        </div>
                    </div>
                </div>

@endsection