@extends('layouts.app')
@section('content')

                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="card">
                                            <div style="padding:15px" class="card-header card-header-icon" data-background-color><a href="agregar.html"  rel="tooltip" title="Agregar"><i class="material-icons">assignment</i></a>
                                        </div>
                                        <div class="card-content">
                                            <h4 class="card-title">Lista De Usuarios</h4>
                                            <div class="toolbar">
                                                <!--        Here you can write extra buttons/actions for the toolbar              -->
                                            </div>
                                            <div class="material-datatables">
                                                <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                                    <thead>
                                                        <tr>
                                                            <th class="disabled-sorting ">Nombre</th>
                                                            <th class="disabled-sorting ">Correo</th>
                                                            <th class="disabled-sorting text-right">Editar</th>
                                                            <th class="disabled-sorting text-right">Cambiar contraseña</th>
                                                            <th class="disabled-sorting text-right">Eliminar</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach ($users as $user)
                                                        <tr>
                                                            <td>{{ $user->name }}</td>
                                                            <td>{{ $user->email }}</td>
                                                            <td class="td-actions text-right">
                                                                <a href="edit_user.html" type="button" rel="tooltip" class="btn btn-success btn-simple">
                                                                    <i class="material-icons">edit</i>
                                                                </a>
                                                            </td>
                                                            <td class="td-actions text-right">
                                                                <a href="change_pass.html" type="button" rel="tooltip" class="btn btn-success btn-simple">
                                                                    <i class="material-icons">https</i>
                                                                </a>
                                                            </td>
                                                            <td style="width: 10%" class="td-actions text-right">
                                                                <button  onclick="demo.showSwal('warning-message-and-confirmation')" type="button" rel="tooltip" class="btn btn-danger btn-simple">
                                                                <i class="material-icons">close</i>
                                                                </button>
                                                            </td>
                                                        </tr>
                                                        @endforeach                                                        
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <!-- end content-->
                                    </div>
                                    <!--  end card  -->
                                </div>
                                <!-- end col-md-12 -->
                            </div>
                            <!-- end row -->
                        </div

@endsection