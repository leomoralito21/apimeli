@extends('layouts.app')
@section('content')

                                    <div class="container">
                                        <div class="row">
                                            <div class="col-md-4 col-sm-6 col-md-offset-3 col-sm-offset-3">
                                                <form action="{{route('users.update', $user['id'])}}" method="POST" autocomplete="on">
                                                    <input type="hidden" name="_method" value="PATCH">
                                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                                    <div class="card card-login card-hidden">
                                                        <div class="card-header text-center" data-background-color>
                                                            <h4 class="card-title">Editar Usuario</h4>
                                                        </div>
                                                        
                                                        <div class="card-content">
                                                            <div class="input-group">
                                                                <span class="input-group-addon">
                                                                    <i class="material-icons">face</i>
                                                                </span>
                                                                <div class="form-group label-floating">
                                                                    <label class="control-label">Nombre</label>
                                                                    <input type="text" name="name" required="required" class="form-control" value="{{ $user['name'] }}" >
                                                                </div>
                                                            </div>
                                                            
                                                            
                                                            <div class="input-group">
                                                                <span class="input-group-addon">
                                                                    <i class="material-icons">email</i>
                                                                </span>
                                                                <div class="form-group label-floating">
                                                                    <label class="control-label">Correo</label>
                                                                    <input id="email" name="email" required="required" type="text" placeholder="correo@mail.com" value="{{ $user['email'] }}"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="card-content footer text-center">
                                                            <div class="row">
                                                                <div class=" col-xs-12  col-md-12">
                                                                    <div class=" col-md-4 col-md-offset-1">
                                                                        <button type="submit" class="btn">Guardar</button>
                                                                    </div>
                                                                    <div class="col-md-4 col-md-offset-1">
                                                                        <a href="listar_user.html" class="btn">Cancelar</a>
                                                                    </div>
                                                                </div>
                                                                
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>

@endsection