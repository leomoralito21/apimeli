<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8" />
        <link rel="apple-touch-icon" sizes="76x76" href="../../assets/img/apple-icon.png" />
        <link rel="icon" type="image/png" href="../../assets/img/favicon.png" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <title>Material Dashboard</title>
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <meta name="viewport" content="width=device-width" />
        <!-- Bootstrap core CSS     -->
        <link href="../../assets/css/bootstrap.min.css" rel="stylesheet" />
        <!--  Material Dashboard CSS    -->
        <link href="../../assets/css/material-dashboard.css" rel="stylesheet" />
        <!--  CSS for Demo Purpose, don't include it in your project     -->
        <link href="../../assets/css/demo.css" rel="stylesheet" />
        <!--     Fonts and icons     -->
        <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons" />
    </head>
    <body>
        <div class="wrapper">
            <div class="sidebar" data-active-color="rose" data-background-color="black" data-image="../../assets/img/sidebar-3.jpg">
                <div class="logo">
                    <a href="" class="simple-text">
                        Texto
                    </a>
                </div>
                <div class="logo logo-mini">
                    <a href="" class="simple-text">
                        Texto
                    </a>
                </div>
                <div class="sidebar-wrapper">
                    <div class="user">
                        <div class="photo">
                            <img src="../../assets/img/default-avatar.png" />
                        </div>
                        <div class="info">
                            <p class="text-usuario">Usuario</p>
                        </div>
                    </div>
                    <ul class="nav">
                        <li class="padding-list">
                            <div class="info">
                                <a data-toggle="collapse" href="#collapse1" class="collapsed">
                                    <i class="material-icons">reorder</i>
                                    <p class="text-list">Categorías</p>
                                </a>
                                <div class="collapse" id="collapse1">
                                    <ul class="nav">
                                        <li>
                                            <a href="../../pages/listar/listar.html">Listado De Categorías</a>
                                        </li>
                                        <li>
                                            <a href="../../pages/listar/agregar.html">Agregar Nueva Categorías</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <li class="padding-list">
                            <div class="info">
                                <a data-toggle="" href="../listar/carga_masiva.html" class="">
                                    <i class="material-icons">publish</i>
                                    <p class="text-list">Carga Masiva</p>
                                </a>
                            </li>
                            <li class="padding-list">
                                <div class="info">
                                    <a data-toggle="collapse" href="#collapse2" class="collapsed">
                                        <i class="material-icons"><i class="material-icons">supervisor_account</i></i>
                                        <p class="text-list">Usuario</p>
                                    </a>
                                    <div class="collapse" id="collapse2">
                                        <ul class="nav">
                                            <li>
                                                <a href="../login/listar_user.html">Lista De Usuarios</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </li>
                            <li class="padding-list">
                                <div class="info">
                                    <a data-toggle="" href="login.html" class="collapsed">
                                        <i class="material-icons"><i class="material-icons">exit_to_app</i></i>
                                        <p class="text-list">Salir</p>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="main-panel">
                        <nav class="navbar navbar-transparent navbar-absolute">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            </button>
                            <div class="container-fluid">
                                <div class="collapse navbar-collapse">
                                    <ul class="nav navbar-nav navbar-right">
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                <i class="material-icons ico_nav">notifications</i>
                                                <span class="notification">5</span>
                                                <p class="hidden-lg hidden-md">
                                                    Notifications
                                                    <b class="caret"></b>
                                                </p>
                                            </a>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <a href="#">Mike John responded to your email</a>
                                                </li>
                                                <li>
                                                    <a href="#">You have 5 new tasks</a>
                                                </li>
                                                <li>
                                                    <a href="#">You're now friend with Andrew</a>
                                                </li>
                                                <li>
                                                    <a href="#">Another Notification</a>
                                                </li>
                                                <li>
                                                    <a href="#">Another One</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                <i class="material-icons ico_nav">person</i>
                                                <p class="hidden-lg hidden-md">
                                                    Más
                                                </p>
                                            </a>
                                            <ul class="dropdown-menu">
                                                <div class="user hidden-sm hidden-xs">
                                                    <div class="photo">
                                                        <img class="img_dropdown" src="../../assets/img/default-avatar.png" />
                                                    </div>
                                                    <div class="info">
                                                        <p class="text-usuario">Usuario</p>
                                                    </div>
                                                </div>
                                                <li><a href="#!"><i class="material-icons ico_dropdown">settings_applications</i>Configuración</a></li>
                                                <li><a href="#!"><i class="material-icons ico_dropdown"><i class="material-icons">lock_outline</i></i>Cambiar clave</a></li>
                                                <li><a href="login/login.html"><i class="material-icons ico_dropdown">exit_to_app</i>Salir</a></li>
                                            </ul>
                                        </li>
                                        <li class="separator hidden-lg hidden-md"></li>
                                    </ul>
                                </div>
                            </div>
                        </nav>
                        
                        
                        <div class="content">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-4 col-sm-6 col-md-offset-3 col-sm-offset-3">
                                        <form method="#" action="#">
                                            <div class="card card-login card-hidden">
                                                <div class="card-header text-center" data-background-color>
                                                    <h4 class="card-title">Cambiar Contraseña</h4>
                                                </div>
                                                
                                                <div class="card-content">
                                                    <div class="input-group">
                                                        <span class="input-group-addon">
                                                            <i class="material-icons">face</i>
                                                        </span>
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">Nombre</label>
                                                            <input type="text" class="form-control" disabled>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="input-group">
                                                        <span class="input-group-addon">
                                                            <i class="material-icons">lock_outline</i>
                                                        </span>
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">Contraseña Actual</label>
                                                            <input type="password" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="input-group">
                                                        <span class="input-group-addon">
                                                            <i class="material-icons">lock_outline</i>
                                                        </span>
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">Contraseña Nueva</label>
                                                            <input type="password" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="card-content footer text-center">
                                                    <div class="row">
                                                        <div class=" col-xs-12  col-md-12">
                                                            <div class=" col-md-4 col-md-offset-1">
                                                                <button type="submit" class="btn">Guardar</button>
                                                            </div>
                                                            <div class="col-md-4 col-md-offset-1">
                                                                <a href="listar_user.html" class="btn">Cancelar</a>
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </body>
                <!--   Core JS Files   -->
                <script src="{{ asset ('js/jquery-3.1.1.min.js') }}" type="text/javascript"></script>
                <script src="{{ asset ('js/jquery-ui.min.js') }}" type="text/javascript"></script>
                <script src="{{ asset ('js/bootstrap.min.js') }}" type="text/javascript"></script>
                <script src="{{ asset ('js/material.min.js') }}" type="text/javascript"></script>
                <script src="{{ asset ('js/perfect-scrollbar.jquery.min.js') }}" type="text/javascript"></script>
                <!-- Forms Validations Plugin -->
                <script src="{{ asset ('js/jquery.validate.min.js') }}"></script>
                <!--  Plugin for Date Time Picker and Full Calendar Plugin-->
                <script src="{{ asset ('js/moment.min.js') }}"></script>
                <!--  Charts Plugin -->
                <script src="{{ asset ('js/chartist.min.js') }}"></script>
                <!--  Plugin for the Wizard -->
                <script src="{{ asset ('js/jquery.bootstrap-wizard.js') }}"></script>
                <!--  Notifications Plugin    -->
                <script src="{{ asset ('js/bootstrap-notify.js') }}"></script>
                <!-- DateTimePicker Plugin -->
                <script src="{{ asset ('js/bootstrap-datetimepicker.js') }}"></script>
                <!-- Vector Map plugin -->
                <script src="{{ asset ('js/jquery-jvectormap.js') }}"></script>
                <!-- Sliders Plugin -->
                <script src="{{ asset ('js/nouislider.min.js') }}"></script>
                <!--  Google Maps Plugin    -->
                <script src="https://maps.googleapis.com/maps/api/js"></script>
                <!-- Select Plugin -->
                <script src="{{ asset ('js/jquery.select-bootstrap.js') }}"></script>
                <!--  DataTables.net Plugin    -->
                <script src="{{ asset ('js/jquery.datatables.js') }}"></script>
                <!-- Sweet Alert 2 plugin -->
                <script src="{{ asset ('js/sweetalert2.js') }}"></script>
                <!--    Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
                <script src="{{ asset ('js/jasny-bootstrap.min.js') }}"></script>
                <!--  Full Calendar Plugin    -->
                <script src="{{ asset ('js/fullcalendar.min.js') }}"></script>
                <!-- TagsInput Plugin -->
                <script src="{{ asset ('js/jquery.tagsinput.js') }}"></script>
                <!-- Material Dashboard javascript methods -->
                <script src="{{ asset ('js/material-dashboard.js') }}"></script>
                <!-- Material Dashboard DEMO methods, don't include it in your project! -->
                <script src="{{ asset ('js/demo.js') }}"></script>
                <script type="text/javascript">
                $().ready(function() {
                demo.checkFullPageBackgroundImage();
                setTimeout(function() {
                // after 1000 ms we add the class animated to the login/register card
                $('.card').removeClass('card-hidden');
                }, 700)
                });
                </script>
            </html>