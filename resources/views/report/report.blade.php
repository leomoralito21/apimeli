<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('img/apple-icon.png') }}" />
    <link rel="icon" type="image/png" href="{{ asset('img/favicon.png" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title></title>
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
    <meta name="viewport" content="width=device-width" />
    <!-- Bootstrap core CSS     -->
    <link href="{{ asset ('css/bootstrap.min.css') }}" rel="stylesheet" />
    <!--  Material Dashboard CSS    -->
    <link href="{{ asset('css/material-dashboard.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/demo.css') }}" rel="stylesheet" />
    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="{{ asset('css/demo.css') }}" rel="stylesheet" />
    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons" />
</head>
<body>
    <div class="wrapper">
        <div class="sidebar" data-active-color="rose" data-background-color="black" data-image="{{ asset('img/sidebar-4.jpg') }}">
            <div class="logo">
                <a href="" class="simple-text">
                    Texto
                </a>
            </div>
            <div class="logo logo-mini">
                <a href="" class="simple-text">
                    Texto
                </a>
            </div>
            <div class="sidebar-wrapper">
                <div class="user">
                    <div class="photo">
                        <img src="{{ asset('img/default-avatar.png') }}" />
                    </div>
                    <div class="info">
                        <p class="text-usuario">Usuario</p>
                    </div>
                </div>
                <ul class="nav">
                    <li class="padding-list">
                        <div class="info">
                            <a  href="../sales/listar_sale.html" class="collapsed">
                                <i class="material-icons">shopping_cart</i>
                                <p class="text-list">Ventas</p>
                            </a>
                        </div>
                    </li>
                    <li class="padding-list">
                        <div class="info">
                            <a data-toggle="" href="../costo/listar_costo.html" class="collapsed">
                                <i class="material-icons">image</i>
                                <p class="text-list">Costo</p>
                            </a>
                        </li>
                        <li class="padding-list">
                            <div class="info">
                                <a data-toggle="collapse" href="#reports" class="collapsed">
                                    <i class="material-icons">assignment</i>
                                    <p class="text-list">Reportes</p>
                                </a>
                                <div class="collapse" id="reports">
                                    <ul class="nav">
                                        <li>
                                            <a href="report.html">Reporte General</a>
                                        </li>
                                        <li>
                                            <a href="report_sale.html">Reporte Ventas</a>
                                        </li>
                                        
                                    </ul>
                                </div>
                            </li>
                            <li class="padding-list">
                                <div class="info">
                                    <a data-toggle="" href="../customer/listar_customer.html" class="">
                                        <i class="material-icons">workt</i>
                                        <p class="text-list">Cliente</p>
                                    </a>
                                </li>
                                <li class="padding-list">
                                    <div class="info">
                                        <a data-toggle="" href="../producto/listar_producto.html" class="collapsed">
                                            <i class="material-icons">view_list</i>
                                            <p class="text-list">
                                                Producto</p>
                                            </a>
                                        </li>
                                        <li class="padding-list">
                                           <div class="info">
                                               <a data-toggle="" href="../login/listar_user.html" class="collapsed">
                                                   <i class="material-icons"><i class="material-icons">supervisor_account</i></i>
                                                   <p class="text-list">Usuario</p>
                                               </a>
                                           </div>
                                       </li>
                                       <li class="padding-list">
                                        <div class="info">
                                            <a data-toggle="" href="../login/login.html" class="collapsed">
                                                <i class="material-icons"><i class="material-icons">exit_to_app</i></i>
                                                <p class="text-list">Salir</p>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="main-panel">
                                <nav class="navbar navbar-transparent navbar-absolute">
                                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                    <div class="container-fluid">
                                        <div class="collapse navbar-collapse">
                                            <ul class="nav navbar-nav navbar-right">
                                                <li class="dropdown">
                                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                        <i class="material-icons ico_nav">notifications</i>
                                                        <span class="notification">5</span>
                                                        <p class="hidden-lg hidden-md">
                                                            Notifications
                                                            <b class="caret"></b>
                                                        </p>
                                                    </a>
                                                    <ul class="dropdown-menu">
                                                        <li>
                                                            <a href="#">Mike John responded to your email</a>
                                                        </li>
                                                        <li>
                                                            <a href="#">You have 5 new tasks</a>
                                                        </li>
                                                        <li>
                                                            <a href="#">You're now friend with Andrew</a>
                                                        </li>
                                                        <li>
                                                            <a href="#">Another Notification</a>
                                                        </li>
                                                        <li>
                                                            <a href="#">Another One</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li class="dropdown">
                                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                        <i class="material-icons ico_nav">person</i>
                                                        <p class="hidden-lg hidden-md">
                                                            Más
                                                        </p>
                                                    </a>
                                                    <ul class="dropdown-menu">
                                                        <div class="user hidden-sm hidden-xs">
                                                            <div class="photo">
                                                                <img class="img_dropdown" src="{{ asset('img/default-avatar.png') }}" />
                                                            </div>
                                                            <div class="info">
                                                                <p class="text-usuario">Usuario</p>
                                                            </div>
                                                        </div>
                                                        <li><a href="#!"><i class="material-icons ico_dropdown">settings_applications</i>Configuración</a></li>
                                                        <li><a href="../login/change_pass.html"><i class="material-icons ico_dropdown"><i class="material-icons">lock_outline</i></i>Cambiar clave</a></li>
                                                        <li><a href="../login/login.html"><i class="material-icons ico_dropdown">exit_to_app</i>Salir</a></li>
                                                    </ul>
                                                </li>
                                                <li class="separator hidden-lg hidden-md"></li>
                                            </ul>
                                        </div>
                                    </div>
                                </nav>
                                <section style="margin-top:50px">
                                    <div class="row">
                                        <div class=" col-xs-7 col-xs-offset-1 col-sm-7 col-sm-offset-1 col-md-7 col-md-offset-1">
                                            <div class="card">
                                                <div class="card-header card-header-icon header_card_icon" data-background-color>
                                                    <i class="material-icons card_icon" aling="center">date_range</i>
                                                </div>
                                                
                                                <div class="col-md-4">
                                                   <div class="card-content">
                                                    <span class="card-title">Fecha Inicio</span>
                                                    <div class="form-group">
                                                        <input type="text" class="form-control datepicker" value="10/10/2018" />
                                                    </div>
                                                </div>

                                            </div>


                                            <div class="col-md-4">
                                               <div class="card-content">
                                                <span class="card-title">Fecha Fin</span>
                                                <div class="form-group">
                                                    <input type="text" class="form-control datepicker" value="10/11/2018" />
                                                </div>
                                            </div>

                                        </div>
                                        <bottom  class="btn pull-right btn_fecha"><i class="material-icons ico_search">search</i></bottom>

                                    </div>


                                </div>
                                <div class="col-md-3 col-xs-3 col-sm-3">
                                 <div class="card">
                                    <div class="card-content">
                                        <span class="card-title">Exportar como:</span>


                                        <div class="row row_card">
                                            <div class="div_export col-xs-2 col-sm-2 col-md-2">
                                                <a href="">
                                                    <img src="{{ asset('svg/svg/pdf.svg') }}" alt="10">
                                                </a>
                                            </div>
                                            <div  class=" div_export col-xs-2 col-sm-2 col-md-2">
                                                <a href="">
                                                    <img  src="{{ asset('svg/svg/excel.svg') }}" alt="10">
                                                </a>
                                            </div>

                                        </div>                                           
                                    </div>

                                </div>

                            </div>
                        </div>
                    </section>


                    <div class="content">
                        <div class="row">
                            <div class="col-sm-6 col-lg-6">
                                <div class="card">
                                    <div class="card-header card-header-text" data-background-color="orange">
                                        <h4 class="card-title">Reporte Ganancias</h4>
                                        <p class="category">New report on 15th September, 2017</p>
                                    </div>
                                    <div class="card-content table-responsive">
                                        <table class="table table-hover">
                                            <thead class="text-warning">
                                                <th>Mes</th>
                                                <th>Total S/IVA</th>
                                                <th>Ganacia S/IVA</th>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Enero</td>
                                                    <td>45677</td>
                                                    <td>$36,738</td>
                                                </tr>
                                                <tr>
                                                    <td>Febrero</td>
                                                    <td>45677</td>
                                                    <td>$36,738</td>
                                                </tr>
                                                <tr>
                                                    <td>Marzo</td>
                                                    <td>45677</td>
                                                    <td>$36,738</td>
                                                </tr>
                                                <tr>
                                                    <td>Abril</td>
                                                    <td>45677</td>
                                                    <td>$36,738</td>
                                                </tr>
                                                <tr>
                                                    <td>Mayo</td>
                                                    <td>45677</td>
                                                    <td>$36,738</td>
                                                </tr>
                                                <tr>
                                                    <td>Junio</td>
                                                    <td>45677</td>
                                                    <td>$36,738</td>
                                                </tr>
                                                <tr>
                                                    <td>Julio</td>
                                                    <td>45677</td>
                                                    <td>$36,738</td>
                                                </tr>
                                                <tr>
                                                    <td>Agosto</td>
                                                    <td>45677</td>
                                                    <td>$36,738</td>
                                                </tr>
                                                <tr>
                                                    <td>Septiembre</td>
                                                    <td>45677</td>
                                                    <td>$36,738</td>
                                                </tr>
                                                <tr>
                                                    <td>Octubre</td>
                                                    <td>45677</td>
                                                    <td>$36,738</td>
                                                </tr>
                                                <tr>
                                                    <td>Noviembre</td>
                                                    <td>45677</td>
                                                    <td>$36,738</td>
                                                </tr>
                                                <tr>
                                                    <td>Diciembre</td>
                                                    <td>45677</td>
                                                    <td>$36,738</td>
                                                </tr>
                                                <tr class="text-warning">
                                                    <td>Total General</td>
                                                    <td>4565677</td>
                                                    <td>$3656566,738</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>


                            <div class="col-sm-6 col-lg-6">

                                <div class="card">
                                    <div class="card-header card-header-icon" data-background-color="rose">
                                        <i class="material-icons">insert_chart</i>
                                    </div>
                                    <div class="card-content">
                                        <h4 class="card-title">Reporte 
                                            <small>- Información Detallada</small>
                                        </h4>
                                    </div>
                                    <div id="multipleBarsChart" class="ct-chart"></div>
                                </div>

                            </div>
                        </div> 
                    </div>



                </body>
                <!--   Core JS Files   -->
                <script src="{{ asset('js/jquery-3.1.1.min.js') }}" type="text/javascript"></script>
                <script src="{{ asset('js/jquery-ui.min.js') }}" type="text/javascript"></script>
                <script src="{{ asset('js/bootstrap.min.js') }}" type="text/javascript"></script>
                <script src="{{ asset('js/material.min.js') }}" type="text/javascript"></script>
                <script src="{{ asset('js/perfect-scrollbar.jquery.min.js') }}" type="text/javascript"></script>
                <!-- Forms Validations Plugin -->
                <script src="{{ asset('js/jquery.validate.min.js') }}"></script>
                <!--  Plugin for Date Time Picker and Full Calendar Plugin-->
                <script src="{{ asset('js/moment.min.js') }}"></script>
                <!--  Charts Plugin -->
                <script src="{{ asset('js/chartist.min.js') }}"></script>
                <!--  Plugin for the Wizard -->
                <script src="{{ asset('js/jquery.bootstrap-wizard.js') }}"></script>
                <!--  Notifications Plugin    -->
                <script src="{{ asset('js/bootstrap-notify.js') }}"></script>
                <!-- DateTimePicker Plugin -->
                <script src="{{ asset('js/bootstrap-datetimepicker.js') }}"></script>
                <!-- Vector Map plugin -->
                <script src="{{ asset('js/jquery-jvectormap.js') }}"></script>
                <!-- Sliders Plugin -->
                <script src="{{ asset('js/nouislider.min.js') }}"></script>
                <!--  Google Maps Plugin    -->
                <script src="https://maps.googleapis.com/maps/api/js"></script>
                <!-- Select Plugin -->
                <script src="{{ asset('js/jquery.select-bootstrap.js') }}"></script>
                <!--  DataTables.net Plugin    -->
                <script src="{{ asset('js/jquery.datatables.js') }}"></script>
                <!-- Sweet Alert 2 plugin -->
                <script src="{{ asset('js/sweetalert2.js') }}"></script>
                <!--    Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
                <script src="{{ asset('js/jasny-bootstrap.min.js') }}"></script>
                <!--  Full Calendar Plugin    -->
                <script src="{{ asset('js/fullcalendar.min.js') }}"></script>
                <!-- TagsInput Plugin -->
                <script src="{{ asset('js/jquery.tagsinput.js') }}"></script>
                <!-- Material Dashboard javascript methods -->
                <script src="{{ asset('js/material-dashboard.js') }}"></script>
                <!-- Material Dashboard DEMO methods, don't include it in your project! -->
                <script src="{{ asset('js/demo.js') }}"></script>
                <script>
                    $(document).ready(function() {
                    demo.initCharts();
                });

                $(document).ready(function() {

                demo.initFormExtendedDatetimepickers();
                });
            </script>

            <script type="text/javascript">
                $(document).ready(function() {
                $('#datatables').DataTable({
                "pagingType": "full_numbers",
                "lengthMenu": [
                [10, 25, 50, -1],
                [10, 25, 50, "All"]
                ],
                responsive: true,
                language: {
                search: "_INPUT_",
                searchPlaceholder: "Search records",
            }
        });
        var table = $('#datatables').DataTable();
        // Edit record
        table.on('click', '.edit', function() {
        $tr = $(this).closest('tr');
        var data = table.row($tr).data();
        alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
    });
    // Delete a record
    table.on('click', '.remove', function(e) {
    $tr = $(this).closest('tr');
    table.row($tr).remove().draw();
    e.preventDefault();
});
//Like record
table.on('click', '.like', function() {
alert('You clicked on Like button');
});
$('.card .material-datatables label').addClass('form-group');
});
</script>
</html>