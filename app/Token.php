<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Token extends Model
{
    protected $fillable = [
        'access_token', 'code', 'site_id', 'app_id', 'secret_key', 'refresh_token', 'user_id'
    ];
}
