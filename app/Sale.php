<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    protected $fillable = [
        'code_facetura', 'type_factura', 'mes_nota_venta', 'mes_facturado', 'total_c_iva', 'total_s_iva', 'nro_client', 'client_id', 'products_id', 'codigo', 'cantidad', 'precio_unit_c_iva', 'precio_unit_s_iva', 'comision_vend', 'comision_gerencia', 'comision_1_en_porcentaje', 'comision_2_en_porcentaje', 'ganancia_unit', 'ganancia_total_s_iva', 'renta_costo', 'renta_factura', 'edo_entrega', 'edo_cobro', 'nro_d_nv', 'vendedor', 'amount_total', 'status_paid'
    ];

    
    public function product()
    {
        return $this->belongsTo('App\Product', 'products_id');
    }

    public function client()
    {
        return $this->belongsTo('App\Client', 'client_id');
    }
}
