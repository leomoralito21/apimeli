<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailInvoice extends Model
{
    protected $fillable = [
 	'invoice_id', 'product_id', 'quantity', 'description', 'code_sku', 'price_unit', 'total'        
    ];

    public function invoices()
    {
        return $this->belongsTo('App\Invoice', 'invoice_id');
    }

}
