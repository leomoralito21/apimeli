<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'description', 'code_product', 'cantidad', 'precio_unit_c_iva', 'precio_unit_s_iva', 'costo_unit, detail_invoices_id'
    ];

    public function detail_invoices()
    {
        return $this->belongsTo('App\DetailInvoice', 'detail_invoices_id');
    }

}
