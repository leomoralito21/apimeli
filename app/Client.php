<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $fillable = [
        'code_client', 'name_client', 'address', 'code_postal', 'cuit', 'iva', 'type_factura', 'n_iibb', 'code_inscription',
    ];
}
