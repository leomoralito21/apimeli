<?php

namespace App\Http\Controllers;



use Illuminate\Http\Request;
use App\Meli;
use App\configApp;
use App\Http\Controlles\HomeController;
use App\Token;
use App\Orders;
use App\Items;

class TokenController extends Controller
{
    
    public function index()
    {   
        $redirectURI = "http://localhost/api_meli/public/dashboard";
        $siteId = 'MLV';
        $meli = new Meli('6445452905791726', 'MBWCsBC3OZzCqaRA4rU3UY55lo6NPkzx', $redirectURI, $siteId);
        $secret_key = "MBWCsBC3OZzCqaRA4rU3UY55lo6NPkzx";
        $app_id = "6445452905791726";
        
        //print_r($_GET['code']);
           

        if(isset($_GET['code']) || isset($_SESSION['access_token'])) {

            // If code exist and session is empty
            if(isset($_GET['code']) && !isset($_SESSION['access_token'])) {
                // //If the code was in get parameter we authorize
                try{
                    $user = $meli->authorize($_GET["code"], $redirectURI);
                    echo '<pre>';
                    print_r($user);
                    echo '</pre>';                
                    // Now we create the sessions with the authenticated user
                    $_SESSION['access_token'] = $user['body']->access_token;
                    $_SESSION['expires_in'] = time() + $user['body']->expires_in;
                    $_SESSION['refresh_token'] = $user['body']->refresh_token;
                }catch(Exception $e){
                    echo "Exception: ",  $e->getMessage(), "\n";
                }
            } else {
                // We can check if the access token in invalid checking the time
                if($_SESSION['expires_in'] < time()) {
                    try {
                        // Make the refresh proccess
                        $refresh = $meli->refreshAccessToken();

                        // Now we create the sessions with the new parameters
                        $_SESSION['access_token'] = $refresh['body']->access_token;
                        $_SESSION['expires_in'] = time() + $refresh['body']->expires_in;
                        $_SESSION['refresh_token'] = $refresh['body']->refresh_token;
                    } catch (Exception $e) {
                        echo "Exception: ",  $e->getMessage(), "\n";
                    }
                }
            }
            $id_user = $user['body']->user_id;
            $token = $user['body']->access_token;
            $refresh_token = $user['body']->refresh_token;
            $token_model = new Token;
            $token_model->access_token = $token;
            $token_model->refresh_token = $refresh_token;
            $token_model->user_id = $id_user;
            $token_model->app_id = $app_id;
            $token_model->secret_key = $secret_key;
            $token_model->site_id = $siteId;
            $token_model->code = $_GET['code'];
            $token_model->save();
            $tokenObj = Token::all();
            $last_token = $tokenObj->last();
            echo '<pre>';
                print_r($user);
            echo '</pre>';
            
            
            //}
            

            return view('home');

        } else {
            return redirect()->away($meli->getAuthUrl($redirectURI, Meli::$AUTH_URL[$siteId]));
            
        }
    
    }

        
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function consult_orders()
    {
        
        $dataToken = Token::all();
        $dataToken_last = $dataToken->last();
        $code_seller = $dataToken_last->code;
        $access_token = $dataToken_last->access_token;
        $user_id = $dataToken_last->user_id;
        $time = "T00:00:00.000-00:00";
        $date_start = "2018-01-23" . $time;
        $date_end = "2018-01-27" . $time;
        $redirectURI = "http://localhost/api_meli/public/dashboard";
        $siteId = 'MLV';
        $meli = new Meli('6445452905791726', 'MBWCsBC3OZzCqaRA4rU3UY55lo6NPkzx', $redirectURI, $siteId);

        // echo '<pre>';
        // print_r($dataToken_last);
        // echo '</pre>';

        $params = array('seller' => $user_id, 'order.date_created.from' => $date_start, 'order.date_created.to' => $date_end, 'access_token' => $access_token);

        $querys = $meli->get('orders/search/', $params);


        //echo '<pre>';
        //print_r($querys);
        //echo '</pre>';

        echo "======================================================";
        echo "======================================================";
        echo "======================================================";
        echo "======================================================";
        foreach ($querys as  $value) {

            if(isset($value->results)){

                $results = $value->results;
                $orders = new Orders;
                $itemObj = new Items;
                foreach($results as $order_sale){

                    $orders->order_id = $order_sale->id;
                    $orders->status = $order_sale->status;
                    $orders->id_seller = $order_sale->seller->id;
                    $orders->order_created = $order_sale->date_created;
                    $orders->date_closet = $order_sale->date_closed;
                    $orders->expiration_date = $order_sale->expiration_date;
                    $orders->date_last_update = $order_sale->date_last_updated;
                    $orders->currency_id = $order_sale->currency_id;
                    $orders->total_amount = $order_sale->total_amount;
                    $orders->sub_status = $order_sale->shipping->substatus;
                    $orders->status_shipping = $order_sale->shipping->status;
                    $orders->shipping_id = $order_sale->shipping->id;
                    $orders->service_id = $order_sale->shipping->service_id;
                    $orders->currency_id_shipping = $order_sale->shipping->currency_id;
                    $orders->shipment_type = $order_sale->shipping->shipment_type;
                    $orders->sender_id = $order_sale->shipping->sender_id; 
                    $orders->date_created_shipping = $order_sale->shipping->date_created;
                    $orders->cost_shipping = $order_sale->shipping->cost;
                    $orders->date_first_printed = $order_sale->shipping->date_first_printed;
                    $orders->id_buyer = $order_sale->buyer->id;
                    $orders->nickname_buyer = $order_sale->buyer->nickname;
                    $orders->email_buyer = $order_sale->buyer->email;
                    $orders->area_code_buyer = $order_sale->buyer->phone->area_code;
                    $orders->extension_buyer = $order_sale->buyer->phone->extension;
                    $orders->number_buyer = $order_sale->buyer->phone->number;
                    $orders->first_name_buyer = $order_sale->buyer->first_name;
                    $orders->last_name_buyer = $order_sale->buyer->last_name;
                    $orders->doc_type_buyer = $order_sale->buyer->billing_info->doc_type;
                    $orders->doc_number_buyer = $order_sale->buyer->billing_info->doc_number; 
                    $orders->id_seller = $order_sale->seller->id;
                    $orders->nickname_seller = $order_sale->seller->nickname;
                    $orders->email_seller = $order_sale->seller->email;
                    $orders->area_code_seller = $order_sale->seller->phone->area_code;
                    $orders->extension_seller = $order_sale->seller->phone->extension;
                    $orders->number_seller = $order_sale->seller->phone->number;
                    $orders->first_name_seller = $order_sale->seller->first_name;
                    $orders->last_name_seller = $order_sale->seller->last_name;
                    $orders->save();
                    echo '<pre>';
                    print_r($order_sale);
                    echo '</pre>';
                    $buyer  = $order_sale->buyer;
                    $items = $order_sale->order_items;
                    foreach ($items as $itemNew) {
                       echo '<pre>';
                        if(isset($itemNew->item->title))
                            $itemObj->title_item = $itemNew->item->title;
                            $itemObj->item_id = $itemNew->item->id;
                            $itemObj->category_id = $itemNew->item->category_id;
                            $itemObj->unit_price = $itemNew->unit_price;
                            $itemObj->cantidad = $itemNew->quantity;
                            $itemObj->currency_id = $itemNew->currency_id;
                            //$itemObj->order_id = 1;
                            //print_r($itemObj);
                        echo '</pre>';

                       //$objtNew->comprador = $buyer->name;
                       //$objetNew->save();
                    //                         (
                    //     [id] => 70939643
                    //     [nickname] => PAZRICARDO74
                    //     [email] => rpaz.lkttct+2-oge3dcnjqgeztkmzr@mail.mercadolibre.com
                    //     [phone] => stdClass Object
                    //         (
                    //             [area_code] => 
                    //             [extension] => 
                    //             [number] => 01120857213
                    //             [verified] => 
                    //         )

                    //     [alternative_phone] => stdClass Object
                    //         (
                    //             [area_code] => 
                    //             [extension] => 
                    //             [number] => 
                    //         )

                    //     [first_name] => Ricardo
                    //     [last_name] => Paz
                    //     [billing_info] => stdClass Object
                    //         (
                    //             [doc_type] => 
                    //             [doc_number] => 
                    //         )

                    // )
                    }
                $itemObj->save();

                }
            }
        }
        
        echo "======================================================";
        echo "======================================================";
        echo "======================================================";
    }


    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
