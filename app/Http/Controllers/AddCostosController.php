<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Costo;

class AddCostosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $costos = Costo::all();
        return view('costo.listar_costo', compact('costos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('costo.add_costo');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Costo::create([
            'description' => $request->input('description'),
            'code' => $request->input('code'),
            'venta' => $request->input('venta'),
        ]);
        return back()->with('success', 'Costo agregado');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $costo = Costo::find($id);
        return view('costo.ver_costo', compact('costo'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $costo = Costo::find($id);
        return view('costo.edit_costo', compact('costo', 'id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $costo = Costo::find($id);
        $this->validate(request(), [
        'code' => 'required|numeric',
        'description' => 'required',
        'venta' => 'required|numeric'
        ]);

        $costo->code = $request->get('code');
        $costo->description = $request->get('description');
        $costo->venta = $request->get('venta');
        $costo->save();

        return redirect('costos')->with('success', 'Costo Actualizado');    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
