<?php

namespace App\Http\Controllers;

session_start();


use Illuminate\Http\Request;
use App\Meli;
use App\configApp;
use App\Sale;

class MeliController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $redirectURI = "http://localhost/api_meli/public/dashboard";
        $siteId = 'MLV';
        $meli = new Meli('6445452905791726', 'MBWCsBC3OZzCqaRA4rU3UY55lo6NPkzx', $redirectURI, $siteId);

        

        if(isset($_GET['code']) || isset($_SESSION['access_token'])) {
            // If code exist and session is empty
            if(isset($_GET['code']) && !isset($_SESSION['access_token'])) {
                // //If the code was in get parameter we authorize
                try{
                    $user = $meli->authorize($_GET["code"], $redirectURI);
                    
                    // Now we create the sessions with the authenticated user
                    $_SESSION['access_token'] = $user['body']->access_token;
                    $_SESSION['expires_in'] = time() + $user['body']->expires_in;
                    $_SESSION['refresh_token'] = $user['body']->refresh_token;
                }catch(Exception $e){
                    echo "Exception: ",  $e->getMessage(), "\n";
                }
            } else {
                // We can check if the access token in invalid checking the time
                if($_SESSION['expires_in'] < time()) {
                    try {
                        // Make the refresh proccess
                        $refresh = $meli->refreshAccessToken();

                        // Now we create the sessions with the new parameters
                        $_SESSION['access_token'] = $refresh['body']->access_token;
                        $_SESSION['expires_in'] = time() + $refresh['body']->expires_in;
                        $_SESSION['refresh_token'] = $refresh['body']->refresh_token;
                    } catch (Exception $e) {
                        echo "Exception: ",  $e->getMessage(), "\n";
                    }
                }
            }

            echo '<pre>';
                
            echo '</pre>';

        } else {
            echo '<a href="' . $meli->getAuthUrl($redirectURI, Meli::$AUTH_URL[$siteId]) . '">Login using MercadoLibre oAuth 2.0</a>';
            
        }

    }

    public function getConsulte($access_token, $meli)
    {
        $time = "T00:00:00.000-00:00";
        $date_start = "2018-01-01" . $time;
        $date_end = "2018-01-27" . $time;
        $seller_id = "147586314";
        $access_token = "APP_USR-6445452905791726-012722-b8d17b3648be82fad7ae332527506d52__L_A__-147586314";
        $redirectURI = "http://localhost/api_meli/public/dashboard";
        $siteId = 'MLV';
        $id_app = "6445452905791726";
        $secret_key_app = "MBWCsBC3OZzCqaRA4rU3UY55lo6NPkzx";

        $params = array('seller' => "147586314", 'order.date_created.from' => $date_start, 'order.date_created.to' => $date_end, 'access_token' => $access_token);

        $querys = $meli->get('orders/search/', $params);


        echo '<pre>';
        //print_r($querys);
        echo '</pre>';

        echo "======================================================";
        echo "======================================================";
        echo "======================================================";
        echo "======================================================";
        foreach ($querys as  $value) {

            if(isset($value->results)){

                $results = $value->results;

                echo '<pre>';
                
                echo '</pre>';

                foreach($results as $result){

                    $seller = $result->seller;
                    $buyer  = $result->buyer;
                    $objetNew = new Sale;




                    $items = $result->order_items;

                    echo '<pre>';
                    echo '</pre>';

                    foreach ($items as $itemNew) {
                       echo '<pre>';
                        if(isset($itemNew->item->title))
                       echo '</pre>';

                       //$objtNew->comprador = $buyer->name;
                       //$objetNew->save();
                    }


                }
            }


        }
        echo "======================================================";
        echo "======================================================";
        echo "======================================================";
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
      */
    public function home()
    {    
        // $redirectURI = "http://localhost/api_meli/public/dashboard";
        // $siteId = 'MLV';
        // $id_app = "6445452905791726";
        // $secret_key_app = "MBWCsBC3OZzCqaRA4rU3UY55lo6NPkzx";
        // $meli = new Meli('6445452905791726', 'MBWCsBC3OZzCqaRA4rU3UY55lo6NPkzx', $redirectURI, $siteId);
        // $_GET = $request->input('code');
        // $params = array('grant_type' => "authorization_code", 'client_id' => $id_app, 'client_secret' => $secret_key_app, 'code' => $_GET, 'redirect_uri' => $redirectURI);
        // $get_token = $meli->post('oauth/token/', $params);
       
        // //$_SESSION = $request->input('access_token');
        // if(isset($_GET['code']) || isset($_SESSION['access_token'])) {
        //     // If code exist and session is empty
        //     if(isset($_GET['code']) && !isset($_SESSION['access_token'])) {
        //         // //If the code was in get parameter we authorize
        //         try{
        //             $user = $meli->authorize($_GET["code"], $redirectURI);                    
        //             // Now we create the sessions with the authenticated user
        //             $_SESSION['access_token'] = $user['body']->access_token;
        //             $_SESSION['expires_in'] = time() + $user['body']->expires_in;
        //             $_SESSION['refresh_token'] = $user['body']->refresh_token;
        //         }catch(Exception $e){
        //             echo "Exception: ",  $e->getMessage(), "\n";
        //         }
        //     } else {
        //         // We can check if the access token in invalid checking the time
        //         if($_SESSION['expires_in'] < time()) {
        //             try {
        //                 // Make the refresh proccess
        //                 $refresh = $meli->refreshAccessToken();

        //                 // Now we create the sessions with the new parameters
        //                 $_SESSION['access_token'] = $refresh['body']->access_token;
        //                 $_SESSION['expires_in'] = time() + $refresh['body']->expires_in;
        //                 $_SESSION['refresh_token'] = $refresh['body']->refresh_token;
        //             } catch (Exception $e) {
        //                 echo "Exception: ",  $e->getMessage(), "\n";
        //             }
        //         }
        //     }

        // } else {
        //     return redirect()->away($meli->getAuthUrl($redirectURI, Meli::$AUTH_URL[$siteId]));
                     
        // }
        
        // //$this->getConsulte($access_token, $meli, $user_id);
        
        // return view('home');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
