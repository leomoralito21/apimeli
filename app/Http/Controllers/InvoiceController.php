<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Invoice;
use App\DetailInvoice;
use App\Client;
use App\Product;
use Carbon\Carbon;

class InvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $today = Carbon::now();
        $month = $today->month;
        $year = $today->year;

        //RECIBOS POR CLIENT
        $invoice_client = DB::table('invoices')->where([

            $client_id = ['client_id', '=', 1],
            $seller_id = ['seller_id', '=', 1]

        ])->select(DB::raw('SUM(total) as total'))->first();

        //RECIBOS POR CLIENTS
        $invoices_client = DB::table('detail_invoices')->join('invoices', 'invoices.id', '=', 'detail_invoices.invoice_id')->select(DB::raw('SUM(detail_invoices.total) as client_total'))->groupBy('client_id')->get();
        
        //RECIBOS POR PRODUCTS
        $invoices_product = DB::table('detail_invoices')->join('invoices', 'invoices.id', '=', 'detail_invoices.invoice_id')->select(DB::raw('SUM(detail_invoices.total) as proudct_total'))->groupBy('product_id')->get();
        
        //REPORTE POR MES
        $invoice_month = Invoice::whereMonth('date_pro', '=', $month)->select(DB::raw('SUM(total) as total'))->first();

        if($invoice_month['total']==null){
            $invoice_month['total'] = 0;
        }
        //RECIBOS POR DIA
        $invoice_day = Invoice::whereDate('date_pro', '=', $today->toDateString())->select(DB::raw('SUM(total) as total'))->first();

        if($invoice_day['total']==null){
            $invoice_day['total'] = 0;
        }

        //REPORTE ANUAL POR MES
        $meses = [];
        
        $meses[0] = 0;
        $meses[1] = 0;
        $meses[2] = 0;
        $meses[3] = 0;
        $meses[4] = 0;
        $meses[5] = 0;
        $meses[6] = 0;
        $meses[7] = 0;
        $meses[8] = 0;
        $meses[9] = 0;
        $meses[10] = 0;
        $meses[11] = 0;

        $invoice_year = Invoice::whereYear('date_pro', '=', $year)->get();

        foreach ($invoice_year as $invo_date) {

            $month = date("m",strtotime($invo_date->date_pro));

            if($month == "01")
                $meses[0] = $meses[0] + $invo_date->total;
            if($month == "02")
                $meses[1] = $meses[1] + $invo_date->total;
            if($month == "03")
                $meses[2] = $meses[2] + $invo_date->total;
            if($month == "04")
                $meses[3] = $meses[3] + $invo_date->total;
            if($month == "05")
                $meses[4] = $meses[4] + $invo_date->total;
            if($month == "06")
                $meses[5] = $meses[5] + $invo_date->total;
            if($month == "07")
                $meses[6] = $meses[6] + $invo_date->total;
            if($month == "08")
                $meses[7] = $meses[7] + $invo_date->total;
            if($month == "09")
                $meses[8] = $meses[8] + $invo_date->total;
            if($month == "10")
                $meses[9] = $meses[9] + $invo_date->total;
            if($month == "11")
                $meses[10] = $meses[10] + $invo_date->total;
            if($month == "12")
                $meses[11] = $meses[11] + $invo_date->total;
            
        }

         //REPORTE DE CLIENTES POR MES
        //$meses = [][];

        //$meses[1][1]=0;
        //$meses[1][2]=

       // $meses_c['client'] = null;        


        $clients = Client::all();  
        $meses_c = array();   
        
        foreach ($clients as $client) {

            $meses_c[$client->id] = array();

            $meses_c[$client->id][0] = 0;
            $meses_c[$client->id][1] = 0;
            $meses_c[$client->id][2] = 0;
            $meses_c[$client->id][3] = 0;
            $meses_c[$client->id][4] = 0;
            $meses_c[$client->id][5] = 0;
            $meses_c[$client->id][6] = 0;
            $meses_c[$client->id][7] = 0;
            $meses_c[$client->id][8] = 0;
            $meses_c[$client->id][9] = 0;
            $meses_c[$client->id][10] = 0;
            $meses_c[$client->id][11] = 0;
                
            $invoices = DB::table('invoices')->where('client_id', '=', $client->id)->whereYear('date_pro', '=', $year)->get();

            foreach ($invoices as $invo) {
                    
                //$array = [$meses_c];
                 
                $month = date("m",strtotime($invo->date_pro));
                if($month == "01")
                    $meses_c[$client->id][0] = $meses_c[$client->id][0] + $invo->total;
                if($month == "02")
                    $meses_c[$client->id][1] = $meses_c[$client->id][1] + $invo->total;
                if($month == "03")
                    $meses_c[$client->id][2] = $meses_c[$client->id][2] + $invo->total;
                if($month == "04")
                    $meses_c[$client->id][3] = $meses_c[$client->id][3] + $invo->total;
                if($month == "05")
                    $meses_c[$client->id][4] = $meses_c[$client->id][4] + $invo->total;
                if($month == "06")
                    $meses_c[$client->id][5] = $meses_c[$client->id][5] + $invo->total;
                if($month == "07")
                    $meses_c[$client->id][6] = $meses_c[$client->id][6] + $invo->total;
                if($month == "08")
                    $meses_c[$client->id][7] = $meses_c[$client->id][7] + $invo->total;
                if($month == "09")
                    $meses_c[$client->id][8] = $meses_c[$client->id][8] + $invo->total;
                if($month == "10")
                    $meses_c[$client->id][9] = $meses_c[$client->id][9] + $invo->total;
                if($month == "11")
                    $meses_c[$client->id][10] = $meses_c[$client->id][10] + $invo->total;
                if($month == "12")
                    $meses_c[$client->id][11] = $meses_c[$client->id][11] + $invo->total;



            }
                              
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////



        $clients = Client::all();
        $products = Product::all();  
        $meses_p = array();   
        
        foreach ($clients as $client) {

            $meses_p[$client->id] = array();

            foreach ($products as $product) {
                
                $meses_p[$client->id][$product->id] = array();
                $meses_p[$client->id][$product->id] = array();
                $meses_p[$client->id][$product->id] = array();
                $meses_p[$client->id][$product->id] = array();
                $meses_p[$client->id][$product->id] = array();
                $meses_p[$client->id][$product->id] = array();
                $meses_p[$client->id][$product->id] = array();
                $meses_p[$client->id][$product->id] = array();
                $meses_p[$client->id][$product->id] = array();
                $meses_p[$client->id][$product->id] = array();
                $meses_p[$client->id][$product->id] = array();
                $meses_p[$client->id][$product->id] = array();

                for($i=0; $i<12; $i++)
                {
                    $meses_p[$client->id][$product->id][$i] = 0;
                }
                $invoices_product = DB::table('invoices')->where([
                    ['product_id', '=', $product->id],
                    ['client_id', '=', $client->id]

                ])->join('detail_invoices', 'detail_invoices.id', '=', 'invoices.id')->get();

                foreach ($invoices_product as $invo) {                    
                    
                     
                    $month = date("m",strtotime($invo->date_pro));
                    if($month == "01")
                        $meses_p[$client->id][$product->id][0] = $meses_p[$client->id][$product->id][0] + $invo->total;
                    if($month == "02")
                        $meses_p[$client->id][$product->id][1] = $meses_p[$client->id][$product->id][1] + $invo->total;
                    if($month == "03")
                        $meses_p[$client->id][$product->id][2] = $meses_p[$client->id][$product->id][2] + $invo->total;
                    if($month == "04")
                        $meses_p[$client->id][$product->id][3] = $meses_p[$client->id][$product->id][3] + $invo->total;
                    if($month == "05")
                        $meses_p[$client->id][$product->id][4] = $meses_p[$client->id][$product->id][4] + $invo->total;
                    if($month == "06")
                        $meses_p[$client->id][$product->id][5] = $meses_p[$client->id][$product->id][5] + $invo->total;
                    if($month == "07")
                        $meses_p[$client->id][$product->id][6] = $meses_p[$client->id][$product->id][6] + $invo->total;
                    if($month == "08")
                        $meses_p[$client->id][$product->id][7] = $meses_p[$client->id][$product->id][7] + $invo->total;
                    if($month == "09")
                        $meses_p[$client->id][$product->id][8] = $meses_p[$client->id][$product->id][8] + $invo->total;
                    if($month == "10")
                        $meses_p[$client->id][$product->id][9] = $meses_p[$client->id][$product->id][9] + $invo->total;
                    if($month == "11")
                        $meses_p[$client->id][$product->id][10] = $meses_p[$client->id][$product->id][10] + $invo->total;
                    if($month == "12")
                        $meses_p[$client->id][$product->id][11] = $meses_p[$client->id][$product->id][11] + $invo->total;



                }
            }      
                              
        }
        echo '<pre>';
         print_r($meses_p);
        echo '</pre>'; 



        
        
        
            
        


                // if($month == "01")
                //     $meses_c[0] = $meses_c[0] + $data_client->total;
                // if($month == "02")
                //     $meses_c[1] = $meses_c[1] + $data_client->total;
                // if($month == "03")
                //     $meses_c[2] = $meses_c[2] + $data_client->total;
                // if($month == "04")
                //     $meses_c[3] = $meses_c[3] + $data_client->total;
                // if($month == "05")
                //     $meses_c[4] = $meses_c[4] + $data_client->total;
                // if($month == "06")
                //     $meses_c[5] = $meses_c[5] + $data_client->total;
                // if($month == "07")
                //     $meses_c[6] = $meses_c[6] + $data_client->total;
                // if($month == "08")
                //     $meses_c[7] = $meses_c[7] + $data_client->total;
                // if($month == "09")
                //     $meses_c[8] = $meses_c[8] + $data_client->total;
                // if($month == "10")
                //     $meses_c[9] = $meses_c[9] + $data_client->total;
                // if($month == "11")
                //     $meses_c[10] = $meses_c[10] + $data_client->total;
                // if($month == "12")
                //     $meses_c[11] = $meses_c[11] + $data_client->total;

            
        
        
        return view('report.invoice', compact('invoices'));
   
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
