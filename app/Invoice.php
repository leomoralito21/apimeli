<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
 protected $fillable = [
 	'contact', 'date_deliver', 'date_pro', 'number_invoice', 'residence', 'phone', 'user_ml', 'iva', 'total', 'cuit', 'iibb', 'iibb_type', 'condition', 'seller', 'shipping_address', 'company_shipping', 'seller_id', 'client_id, detailinvoice_id'        
    ];

    public function detail_invoices()
    {
        return $this->hasMany('App\DetailInvoice', 'detailinvoice_id');
    }

    public function client()
    {
        return $this->belongsTo('App\Client', 'client_id');
    }

}
