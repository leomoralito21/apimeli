<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    protected $fillable = [
        'order_id', 'status', 'order_created', 'date_closet', 'expiration_date', 'date_last_update', 'currency_id', 'total_amount', 'sub_status', 'status_shipping', 'shipping_id', 'service_id', 'currency_id_shipping', 'shipment_type', 'sender_id', 'picking_type', 'date_created_shipping', 'cost_shipping', 'date_first_printed', 'id_buyer', 'nickname_buyer', 'email_buyer', 'area_code_buyer', 'extension_buyer', 'number_buyer', 'first_name_buyer', 'last_name_buyer', 'doc_type_buyer', 'doc_number_buyer', 'id_seller', 'nickname_seller', 'email_seller', 'hone_seller', 'area_code_seller', 'extension_seller', 'number_seller', 'first_name_seller', 'last_name_seller'
    ];
}
