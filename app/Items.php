<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Items extends Model
{
    protected $fillable = [
        'item_id', 'category_id', 'title_item', 'unit_price', 'cantidad', 'currency_id'
    ];

    // public function order_id()
    // {
    //     return $this->belongsTo('App\Orders', 'order_id');
    // }
}
