<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Costo extends Model
{
    protected $fillable = [
        'code', 'description', 'venta',
    ];
}
